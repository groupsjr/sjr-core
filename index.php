<?php

namespace sjr;

define( 'SJR_CORE_DIR', __DIR__ );
define( 'SJR_CORE_FILE', __FILE__ );

require __DIR__.'/vendor/autoload.php';

require __DIR__.'/global.php';

require __DIR__.'/array.php';
require __DIR__.'/dev.php';
require __DIR__.'/fixed-order/functions.php';
require __DIR__.'/functions.php';
require __DIR__.'/images.php';
require __DIR__.'/licenses.php';
require __DIR__.'/query.php';
require __DIR__.'/site.php';
require __DIR__.'/template.php';

if( is_admin() )
	require __DIR__.'/admin/index.php';

/**
*	set up actions and filters for items on the 'Dev' tab
*/
function setup_dev(){
	$dev_options = get_sjr_option( 'dev' );
	
	if( !empty($dev_options->deprecated_die) ){
		add_action( 'deprecated_argument_run', __NAMESPACE__.'\deprecated_function_run', 10, 3 );
		add_action( 'deprecated_constructor_run', __NAMESPACE__.'\deprecated_constructor_run', 10, 2 );
		add_action( 'deprecated_function_run', __NAMESPACE__.'\deprecated_function_run', 10, 3 );
	}

	// @TODO make this run on cron, not every admin request
	if( !empty($dev_options->revisions) ){
		add_action( 'admin_init', __NAMESPACE__.'\auto_delete_revisions' );
	}

	// show direct link to media asset
	add_filter( 'media_row_actions', __NAMESPACE__.'\media_row_actions', 10, 3 );
}
add_action( 'plugins_loaded', __NAMESPACE__.'\setup_dev', 0 );

/**
*	set up actions and filters for items on the 'Images' tab
*/
function setup_images(){
	$images_options = get_sjr_option( 'images' );

	// copy from envs
	if( !empty($images_options->copy_images) ){
		Timer::instance();

		add_filter( 'acf/format_value/type=image', __NAMESPACE__.'\acf_images_copy', 20, 3 );
		add_filter( 'image_downsize', __NAMESPACE__.'\image_downsize_copy', 10, 3 );
		add_filter( 'post_thumbnail_html', __NAMESPACE__.'\html_scan_images_copy', 10, 1 );
		add_filter( 'the_content', __NAMESPACE__.'\html_scan_images_copy', 10, 5 );
		add_filter( 'timber_image_src', __NAMESPACE__.'\url_image_copy', 10, 1 );
		add_filter( 'wp_calculate_image_srcset', __NAMESPACE__.'\srcset_images_copy', 10, 5 );
		add_filter( 'wp_get_attachment_url',  __NAMESPACE__.'\url_image_copy', 10, 2 );
	}
	
	// placeholder images for missing images
	if( !empty($images_options->placeholder) ){
		add_filter( 'acf/format_value/type=image', __NAMESPACE__.'\acf_images_placeholder', 20, 3 );
		add_filter( 'acf/format_value/type=wysiwyg', __NAMESPACE__.'\html_scan_images_placeholder', 10, 1 );

		add_filter( 'post_thumbnail_html', __NAMESPACE__.'\post_thumbnail_html_placeholder', 10, 5 );
		add_filter( 'the_content', __NAMESPACE__.'\html_scan_images_placeholder', 10, 1 );
	}

	// force placeholder
	if( !empty($images_options->placeholder) && !empty($images_options->placeholder_force) ){
		add_filter( 'image_downsize', __NAMESPACE__.'\image_downsize_placeholder', 10, 3 );
		add_filter( 'wp_get_attachment_url', __NAMESPACE__.'\url_image_placeholder', 10, 2 );
	}

	// add query string cache breaker
	if( !empty($images_options->query_string) && !is_admin() ){
		add_filter( 'wp_get_attachment_metadata', __NAMESPACE__.'\image_cachebreaker_metadata', 100, 2 );
		add_filter( 'wp_get_attachment_url', __NAMESPACE__.'\image_cachebreaker_url', 10, 2 );
	}

	if( !empty($images_options->query_string_random) && !is_admin() ){
		add_filter( 'wp_get_attachment_metadata', __NAMESPACE__.'\image_cachebreaker_metadata_random', 100, 2 );
		add_filter( 'wp_get_attachment_url', __NAMESPACE__.'\image_cachebreaker_url_random', 10, 2 );
	}

	// copy images from offsite
	if( !empty($images_options->offsite) && count($images_options->offsite_include) ){
		add_filter( 'the_content', __NAMESPACE__.'\images_copy_offsite', 10, 1 );
	}

	// s3
	if( !empty($images_options->s3_copy_replace) ){
		add_action( 'aws_init', function(){
			add_filter( 'posts_results', __NAMESPACE__.'\s3_copy_missing_image', 10, 2 );
		}, 20 );
	}

	// fixes for cloudinary, force regen, and animated gif
	add_action( 'wp_ajax_regeneratethumbnail', __NAMESPACE__.'\regeneratethumbnail_s3_fix', 1 );
	add_filter( 'wp_get_attachment_metadata', __NAMESPACE__.'\wp_get_attachment_metadata_sizes_default', 1000, 2 );
	add_filter( 'wp_get_attachment_url', __NAMESPACE__.'\wp_get_attachment_url', 10, 2 );
	add_filter( 'wp_image_editors', __NAMESPACE__.'\wp_image_editors_animated_gif_fix', 100 );
}
add_action( 'plugins_loaded', __NAMESPACE__.'\setup_images', 0 );

/**
*	
*/
function setup_ordering(){
	$ordering_options = get_sjr_option( 'ordering' );
	
	if( !empty($ordering_options->fixed['on']) ){
		require_once __DIR__.'/fixed-order/index.php';

		add_action( 'post_submitbox_misc_actions', __NAMESPACE__.'\fixed_order\post_submitbox' );
		add_action( 'save_post', __NAMESPACE__.'\fixed_order\save_post', 10, 3 );

		add_filter( 'pre_get_posts', __NAMESPACE__.'\fixed_order\pre_get_posts', 10, 1 );
		add_filter( 'posts_results', __NAMESPACE__.'\fixed_order\posts_results', 10, 2 );
		
		add_action( 'load-post.php', __NAMESPACE__.'\post_styles' );
		add_action( 'load-post-new.php', __NAMESPACE__.'\post_styles' );
	}

	if( !empty($ordering_options->inline_sticky) ){
		add_filter( 'pre_get_posts', __NAMESPACE__.'\pre_get_posts_inline_sticky' );
		add_filter( 'posts_orderby', __NAMESPACE__.'\posts_orderby_inline_sticky', 10, 2 );
	}
}
add_action( 'plugins_loaded', __NAMESPACE__.'\setup_ordering', 0 );

/**
*	set up actions and filters for items on the 'Site' tab
*/
function setup_site(){
	$site_options = get_sjr_option( 'site' );
		
	if( !empty($site_options->adminajax) ){
		add_filter( 'admin_url', __NAMESPACE__.'\rewrite_ajax_admin_url', 10, 3 );
		add_filter( 'pre_get_posts', __NAMESPACE__.'\rewrite_ajax_pre_get_posts', 1, 1 );
		add_filter( 'query_vars', __NAMESPACE__.'\rewrite_ajax_query_vars', 10, 1 );
		add_filter( 'root_rewrite_rules', __NAMESPACE__.'\rewrite_ajax_root_rewrite_rules', 10, 1 );
	}

	if( empty($site_options->adminbar) ){
		add_filter( 'show_admin_bar', '__return_false', 10, 1 );
	}
	
	if( !empty($site_options->async) ){
		global $concatenate_scripts;
		$concatenate_scripts = TRUE;

		add_action( 'print_head_scripts', __NAMESPACE__.'\print_scripts_async', 10 );
		add_action( 'print_footer_scripts', __NAMESPACE__.'\print_scripts_async', 10 );
	}

	if( !empty($site_options->dashboard_items['on']) ){
		add_action( 'load-index.php', __NAMESPACE__.'\dashboard_items' );
	}

	if( !empty($site_options->head) ){
		add_action( 'init', __NAMESPACE__.'\remove_head_elements' );
	}

	if( !empty($site_options->log_email) ){
		add_action( 'phpmailer_init', __NAMESPACE__.'\log_email', 10, 1 );
	}

	if( !empty($site_options->password['on']) && count($site_options->password['users']) ){
		add_action( 'plugins_loaded', __NAMESPACE__.'\http_auth' );
	}

	if( !empty($site_options->serialized_options) ){
		add_action( 'admin_head-options.php', __NAMESPACE__.'\serialized_options_head' );
	}

	if( !empty($site_options->robots) ){
		add_filter( 'option_blog_public', __NAMESPACE__.'\option_blog_public', 10, 1 );
	}

	if( !empty($site_options->posts_per_page['on']) ){
		add_filter( 'pre_get_posts', __NAMESPACE__.'\pre_get_posts_posts_per_page', 1 );
	}

	if( !empty($site_options->title_tags['on']) ){
		add_filter( 'the_title', __NAMESPACE__.'\the_title_title_tags', 10, 2 );
	}

	if( !empty($site_options->validate_content) ){
		add_filter( 'wp_insert_post_data', __NAMESPACE__.'\wp_insert_post_data_validate_html', 10, 2 );
	}

	add_action( 'init', __NAMESPACE__.'\check_w3_cdn' );
}
add_action( 'plugins_loaded', __NAMESPACE__.'\setup_site', 0 );

/**
*	set up actions and filters for items on the 'Taxonomies' tab
*/
function setup_taxonomies(){
	//$site_options = get_sjr_option( 'taxonomies' );

	add_filter( 'wp_terms_checklist_args', __NAMESPACE__.'\wp_terms_checklist_args', 10, 2 );
}
add_action( 'plugins_loaded', __NAMESPACE__.'\setup_taxonomies', 0 );

/**
*	set up actions and filters for items on the 'Tools' tab
*/
function setup_tools(){
	$tools_options = get_sjr_option( 'tools' );

	add_filter( 'option_default_category', __NAMESPACE__.'\option_default_category', 10, 2 );
}
add_action( 'plugins_loaded', __NAMESPACE__.'\setup_tools', 0 );
