<?php

namespace sjr;

/**
*	not ready for prime time
*	attached to `admin_init` action
*	@TODO make this run on cron, not every admin request
*/
function auto_delete_revisions(){
	global $wpdb;

	$sql = "SELECT post_parent AS parent_id, post_title, 
				group_concat(ID ORDER BY ID DESC) AS `revision_ids`, COUNT(ID) AS `total`
			FROM $wpdb->posts 
			WHERE post_type = 'revision'
			AND post_content != ''
			GROUP BY post_content, post_parent
			HAVING `total` > 1
			ORDER BY `total` DESC, post_parent DESC";

	$res = $wpdb->get_results( $sql );

	foreach( $res as &$row ){
		$row->revision_ids = array_map( 'intval', explode(',' , $row->revision_ids) );
	}

	foreach( $res as $row ){
		// leave one
		$revision_ids = array_slice( $row->revision_ids, 1 );
		$revision_ids = implode( ', ', $revision_ids );

		$sql = $wpdb->prepare( "DELETE FROM $wpdb->posts  
								WHERE post_type = 'revision'
								AND post_parent = %d 
								AND ID IN( $revision_ids )", $row->parent_id );
		$res = $wpdb->query( $sql );
	}
}

/**
*	toggled from `Die on deprecated function` setting
*	attached to `deprecated_constructor_run` action
*	@param
*	@param
*/
function deprecated_constructor_run( $class, $version ){
	if( function_exists('dbug') )
		dbug( func_get_args(), 'deprecated_function_run', 100 );
	else
		debug_print_backtrace();

	die();
}


/**
*	toggled from `Die on deprecated function` setting
*	attached to `deprecated_function_run` action
*	@param
*	@param
*	@param
*/
function deprecated_function_run( $function, $replacement, $version ){
	if( function_exists('dbug') )
		dbug( func_get_args(), 'deprecated_function_run', 100 );
	else
		debug_print_backtrace();

	die();
}

/**
*	for forcing mock thumbnail images
*	@param bool
*	@param int
*	@param string
*	@param bool
*	@return bool
*/
function get_post_metadata( $return, $object_id, $meta_key, $single ){
	if( $meta_key == '_thumbnail_id' ){
		$return = TRUE;
	}
	
	return $return;
}
//add_filter( 'get_post_metadata', __NAMESPACE__.'\get_post_metadata', 10, 4 );

/**
*	set username and password correctly in curl requests - may not be needed?
*	@param cURL handle
*	@param array
*	@param string
*/
function http_api_curl( &$handle, $r, $url ){
	$parsed = parse_url( $url );

	if( !empty($url['user']) && !empty($url['pass']) ){
		curl_setopt( $handle, CURLOPT_USERPWD, "{$parsed['user']}:{$parsed['pass']}" );
	}
}
//add_action( 'http_api_curl', __NAMESPACE__.'\http_api_curl', 10, 3 );

