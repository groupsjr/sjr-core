<?php 

/**
*	Functions and classes copied from WP core
*/

require __DIR__.'/lib/WP_Object_Cache.php';
require __DIR__.'/lib/sjr/object-cache.php';

/**
*
*/
function wp_cache_add( $key, $data, $group = '', $expire = 0 ){
	global $wp_object_cache;

	return $wp_object_cache->add( $key, $data, $group, (int) $expire );
}

/**
*
*/
function wp_cache_add_non_persistent_groups( $groups ){
	// Default cache doesn't persist so nothing to do here.
}

/**
*
*/
function wp_cache_close(){
	return TRUE;
}

/**
*
*/
function wp_cache_delete( $key, $group = '' ){
	global $wp_object_cache;

	return $wp_object_cache->delete($key, $group);
}

/**
*
*/
function wp_cache_init(){
	$GLOBALS['wp_object_cache'] = new \sjr\Object_Cache;
}

/**
*
*/
function wp_cache_get( $key, $group = '', $force = false, &$found = null ){
	global $wp_object_cache;
	return $wp_object_cache->get( $key, $group, $force, $found );
}

/**
*
*/
function wp_cache_set( $key, $data, $group = '', $expire = 0 ){
	global $wp_object_cache;

	return $wp_object_cache->set( $key, $data, $group, (int) $expire );
}