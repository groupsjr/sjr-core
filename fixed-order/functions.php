<?php

namespace sjr\fixed_order;

/*
*
*	@param WP_Query
*	@return string
*/
function active_page_type( $wp_query ){
	$ordering_options = \sjr\get_sjr_option( 'ordering' );
	$page_type = FALSE;
	
	foreach( page_types() as $type ){
		if( $wp_query->is_main_query() && call_user_func_array(array($wp_query, 'is_'.$type), array() ) ){
			$page_type = $type;
			break;
		}
	}

	return $page_type;
}

/*
*
*	@param string
*	@param object
*	@return array
*/
function get_fixed_order_pages( $page_type, $queried_object = NULL ){
	global $wpdb;

	static $ordered = array();
	if( !isset($ordered[$page_type]) ){
		$key = "_fixed-order[$page_type]";

		$args = array(
			'ignore_sticky_posts' => TRUE,
			'meta_query' => array(
				array(
					'key' => $key,
					'type' => 'NUMERIC'
				)
			)
		);

		if( !empty($queried_object->term_id) ){
			$args['order'] = 'ASC';
			$args['orderby'] = 'meta_value_num';
			$args['tax_query'] = array(
				array(
					'field' => 'id',
					'taxonomy' => $queried_object->taxonomy,
					'terms' => $queried_object->term_id
				)
			);
		}

		$query = new \WP_Query( $args );
		
		$res = array_map( function($post) use($key){
			return (object) array(
				'post_id' => intval( $post->ID ),
				'ordered_index' => intval( $post->$key )
			);
		}, $query->posts );
	
		$ordered[$page_type] = $res;
	}

	return $ordered[$page_type];
}

/*
*	get the page types fixed oreding supports
*	@return array
*/
function page_types(){
	// needs to be ordered most specefic to least for is_* wp_query functions
	return array( 'category', 'tag',
				  'archive', 'home', 'search'  );
}