<?php

namespace sjr\fixed_order;

/**
*	filter for inline sticky to not query for sticky posts after main query has run
*	@param WP_Query
*	@return WP_Query
*/
function pre_get_posts( $wp_query ){
	$page_type = active_page_type( $wp_query );

	if( !$page_type )
		return $wp_query;

	// all the posts that have a fixed order on query type
	$queried_object = get_queried_object();
	$ordered_posts = get_fixed_order_pages( $page_type, $queried_object );

	$post__not_in = (array) $wp_query->get( 'post__not_in' );

	// exclude all fixed order from query
	$post_ids = wp_list_pluck( $ordered_posts, 'post_id' );
	$post_ids = array_map( 'intval', $post_ids );
	$wp_query->set( 'post__not_in', array_merge($post__not_in, $post_ids) );

	// change offset in query to account for fixed order posts
	$paged = max( intval($wp_query->get('paged')), 1 );

	//
	$posts_per_page = $wp_query->get( 'posts_per_page' );
	if( !$posts_per_page )
		$posts_per_page = (int) get_option( 'posts_per_page' );

	$posts_per_page_pre = $posts_per_page;

	$offset = $offset_pre = ( $posts_per_page * ($paged - 1) );

	foreach( $ordered_posts as $ordered_post ){
		if( ($ordered_post->ordered_index <= $offset_pre) && ($ordered_post->ordered_index > 0) )
			$offset--;
	}

	// change limit in query
	foreach( $ordered_posts as $ordered_post ){
		if( ($ordered_post->ordered_index <= $offset_pre + $posts_per_page_pre) && ($ordered_post->ordered_index > (($paged - 1) * $posts_per_page_pre)) )
			$posts_per_page--;
	}

	//
	$wp_query->set( 'offset', $offset );

	//
	$wp_query->set( 'posts_per_page', $posts_per_page );
	
	//
	$wp_query->set( 'fixed_order_limits', array(
		$offset,
		$posts_per_page,
	) );

	return $wp_query;
}

/*
*
*	@param array
*	@param WP_Query
*	@return array
*/
function posts_results( $posts, $wp_query ){
	$page_type = active_page_type( $wp_query );
	if( !$page_type )
		return $posts;
	
	$ordered = get_fixed_order_pages( $page_type );

	$limits = $wp_query->get( 'fixed_order_limits' );
	if( $limits ){
		foreach( $ordered as $post ){
			if( $post->ordered_index >= $limits[0] && $post->ordered_index <= $limits[1] ){
				// subtract 1 for 0 based vs input position
				array_splice( $posts, $post->ordered_index - 1, 0, array(get_post($post->post_id)) );
			}
		}
	}

	return $posts;
}