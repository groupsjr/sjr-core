<?php

namespace sjr\fixed_order;

/*
*	render UI for fixed position
*/
function post_submitbox(){
	global $post;
	
	$ordering_options = \sjr\get_sjr_option( 'ordering' );

	$page_types = array();
	foreach( page_types() as $page_type ){
		if( $ordering_options->fixed[$page_type] ){
			array_push( $page_types, (object) array(
				'type' => $page_type,
				'value' => get_post_meta( $post->ID, "_fixed-order[$page_type]", TRUE )
			) );
		}
	}

	$vars = array(
		'page_types' => $page_types
	);
	
	echo \sjr\render( 'admin/post-order', $vars );
}

/*
*
*	@param int
*	@param WP_Post
*	@param bool
*	@return
*/
function save_post( $post_id, $post, $update = FALSE ){
	if( !\sjr\is_post_real_save($post) )
        return $post_id;
       
    if( !isset($_POST['fixed-order']) )
    	return $post_id;

    $fixed_order = $_POST['fixed-order'];

    foreach( $fixed_order as $k => $val )
    	if( trim($val) )
			update_post_meta( $post_id, "_fixed-order[$k]", $val ); 
		else
			delete_post_meta( $post_id, "_fixed-order[$k]" );

	return $post_id;
}