jQuery( document ).ready( function($){
	'use strict';

	// insert two spaces on tab key press
	$( 'textarea.yaml' ).keydown( function(e){
		if( e.which == 9 ){
			var val = this.value;
            var start = this.selectionStart;

            this.value = val.slice( 0, this.selectionStart ) + "  " + val.slice( this.selectionEnd );
            this.selectionEnd = this.selectionStart = start + 2;

			return false;
		}
	} );

	// toggle environemnts
	$( 'select#sjr-env' ).change( function(){
		window.location.href = window.location.href + '&env=' + this.value;
	} );

	//
	$( 'input.autotoggle' ).each( function(i, el){
		var $el = $( el );
		var $attached = $el.parents('.toggle-container').next( 'legend' ).find( 'div.autotoggle' );
		console.log( $attached );
		$el.change( function(){
			var active = this.value == 0 ? false : true;
			var $inputs;

			if( active ){
				$inputs = $attached.find( '[readonly]' );
			} else {
				$inputs = $attached.find( 'input, textarea' );
			}
			
			$attached.toggleClass( 'inactive', !active );
			$inputs.attr( 'readonly', !active );
		} );
	} );

	// readonly for checkboxes
	$( ':checkbox' ).click( function(e){
		var $this = $(this);

		if( $this.attr('readonly') )
			e.preventDefault();
	} );

	// add user pass on site password protection
	$( 'button#add_user' ).click( function(e){
		var $item = $( '.add_user_item' ).last();
		var $template = $item.clone( true );

		$template.find( 'input' ).increment_index().val( '' );

		$item.after( $template );

		e.preventDefault();
	} );

	// remove user pass combo on password protection
	$( 'a.remove' ).click( function(e){
		var $a = $( this );
		var $creds = $( 'div.add_user_item' );
		var $div = $a.parents( 'div.add_user_item' );

		if( $creds.length > 1 )
			$div.remove();
		else
			$div.empty_fields();

		e.preventDefault();
	} );

	// set input elements in jquery object to blank
	$.fn.empty_fields = function(){
		this.find( 'input' ).val( '' );

		return this;
	}

	// find input names matching [number] and increment by 1
	$.fn.increment_index = function(){
		this.each( function(i, item){
			console.log( arguments );

			var name = item.name;
			var index = name.match( /[0-9+]/ );

			index = parseInt( index, 10 ) + 1 ;
			name = name.replace( /\[[0-9+]\]/, '['+index+']' );
			item.name = name;

			console.log( 'name', name );
		} );

		return this;
	};
} );