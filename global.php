<?php

/**
*	gets bitly short url for permalink
*	@param string long url to shorten
*	@param string uses from settings page if not provided
*	@param string uses from settings page if not provided
*	@param string uses from settings page if not provided
*	@return string short url
*/
function bitly( $url = '', $access_token = '', $login = '', $api_key = '' ){
	if( !$access_token || (!$login || !$api_key) ){
		$options = sjr\get_sjr_option( 'licenses' );

		$access_token = empty( $access_token ) && isset( $options->bitly_access_token ) ? $options->bitly_access_token : $access_token;
		$api_key = empty( $api_key ) && isset( $options->bitly_api_key ) ? $options->bitly_api_key : $api_key;
		$login = empty( $login ) && isset( $options->bitly_login ) ? $options->bitly_login : $login;
	}

	$query = array_filter( array(
		'access_token' => $access_token,
		'apiKey' => $api_key,
		'login' => $login,
		'longUrl' => $url
	) );

	$transient_key = sjr\transient_key( 'bitly', $query );
	$body = get_transient( $transient_key );

	if( $body === FALSE ){
		$bitly_url = 'https://api-ssl.bitly.com/v3/shorten?'.http_build_query( $query );
		$response = wp_remote_get( $bitly_url );

		if( is_wp_error($response) )
			return FALSE;

		$body = json_decode( wp_remote_retrieve_body($response) );
	
		set_transient( $transient_key, $body, DAY_IN_SECONDS );
	}
	
	switch( $body->status_code ){
		case 200:
			$short_url = $body->data->url;
			break;
			
		case 500:
		default:
			$short_url = $url;
			break;
	}

	return $short_url;
}

/**
*
*	@param string
*	@param string
*	@param bool
*	@return string
*/
function readonly( $checked, $current = true, $echo = true ){
	return __checked_selected_helper( $checked, $current, $echo, 'readonly' );
}