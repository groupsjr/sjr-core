#SJR Core

Version: 1.2.7.2

This plugin provides various utilities to enhance/override default WordPress functionality. It also provides tools to dynamically clean up content after certain types of migrations or configuration changes.

## Settings

Settings are found here in the WordPress admin: `/wp-admin/admin.php?page=sjr-core_dev`

## Dev

### Auto remove same revisions

Removes identical revisions of posts.

### Die on deprecated function

Output stack trace and terminates when `deprecated_constructor_run` or `deprecated_function_run` actions are called.

## Images

### Copy Images

#### Define environments to copy missing images

Specify URL of remote uploads directories on each line.  Will attempt to download missing images from each URL when missing images are detected on local website.

#### Copy to s3 and content replace

If the image is found on another website, will also re-upload the image to S3 if configured.

#### Debug 

Outputs information if `Dbug` plugin is enabled, such as directory being created or number of bytes in image if successful.

### Copy Offsite Images

#### Copy offsite images in post content to `wp-content/uploads`

Enter one hostname per line. Inline images in post content that refer to the given hostnames will be copied to WordPress Media Library and the images sources are replace in content to refer to the local copy.

### Placeholder Images

#### Use placehold.it for missing images

Use a sized placeholder image if image is not found.

#### Force placeholder

Use placeholder image regardless if image is found.

### Image Query Strings

#### Add query string based on image meta to break caching

Generates and adds `?c_url` parameter to image sources with value based on image metadata.

#### Add random query string to break caching

Generates and adds `?r_mdt` parameter to image sources with random value.

## Licenses

Optionally, view and manage 3rd party license keys for various services and plugins.

## Post Ordering

### Fixed Positions

### Inline Sticky Posts

Changes behavior of sticky posts to be included in the page post count rather than appended.

## Site

### Admin Ajax

Sets up a rewrite rule to force requests usually made to `/wp-admin/admin-ajax.php` with `/ajax/`.

### Admin Bar

Toggle to show the admin bar for logged in users viewing the frontend of site.

### Asynchronous JavaScript

Allows JavaScript sources to be registered with `#async` or `#defer` at the end of the path, and dynamically outputs the respective HTML5 attribute `async` or `defer` with the script tag.

### Dashboard Items

Remove typically unneeded items from the admin dashboard.

### `<head>` Elements

Remove "rsd_link", "wlwmanifest_link", and "wp_generator" META tags from HEAD element, normally attached to the `wp_header` action.

Welcome - `Welcome to WordPress!` Links at top of dashboard.

### Log Email

Logs outgoing email sent from `wp_mail` if plugin `Dbug` is installed.

### Options

Shows raw serialized data in `/wp-admin/options.php`.

### Password Protection

PHP implementation of 401 authentication, normally handled by `.htpasswd`.

### Posts Per Page

Offers finer control of default posts per page, by page type, rather than the site-wide setting on `/wp-admin/options-reading.php`.

### robots.txt

Disallow crawlers from crawling the site based on host name. Enter partial host names per line; matching any part will display `/robots.txt` disallowing crawling of site.

### Title Tags

### Transients

*Not implemented.*  Uses `Object_Cache` class in `lib/sjr/object-cache.php` to write transients to custom table rather than options.

### Validation

#### Show `libxml` errors

Toggle PHP [libxml\_use\_internal\_errors](http://php.net/libxml_use_internal_errors). 

#### Validate HTML on post save 

Use PHP `Dom` classes to parse post content and save valid HTML from post editor.

## Taxonomies

### Single Taxonomies

This will replace the standard taxonomy chooser in the post editor with a UI that allows only one term in the given taxonomy to be selected at a time.

## Tools

### Delete Term #1

This will delete the term with id of 1, which is usually "Uncategorized" and protected by WordPress.

### Delete Transients

This will clear all the transients usually stored in `wp_options` table, or from the Object Cache if enabled.

### Fix Broken Images

Scans post content for broken inline images from the WordPress Media Library. Will attempt to replace with an image with the same ID, preferring using a larger size/thumbnail.

## Environment Selector

*Not implemented.* DB, Local, Stage, Prod. Meant to allow different settings between environments.
