<?php
/*
Plugin Name: 	SJR Core
Plugin URI: 
Description:	Core functionaity and dev tools
Author:			Eric Eaglstun
Version:		1.2.7.5
Author URI: 
License: @todo find - free, do not distribute
License URI: 

This file must be parsable by php 5.2
*/

register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );

//
register_activation_hook( __FILE__, '\sjr\add_capabilities' );

require __DIR__.'/index.php';