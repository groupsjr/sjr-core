<?php

namespace sjr;

/**
*	array_map callback to get `id` property of object
*	@param object
*	@return int
*/
function IDs( $r ){
	if( isset($r->ID) )
		return (int) $r->ID;

	return (int) $r->id;
}

/**
*	array_map callback to get `name` property of object
*	@param object
*	@return string
*/
function names( $r ){
	return (string) $r->name;
}

/**
*	array_map callback to get `parent` property of object
*	@param object
*	@return int
*/
function parents( $r ){
	return (int) $r->parent;
}

/**
*	array_map callback to get `post_name` property of object
*	@param object
*	@return int
*/
function post_names( $r ){
	return (string) $r->post_name;
}

/**
*	array_map callback to get `post_parent` property of object
*	@param object
*	@return int
*/
function post_parents( $r ){
	return (int) $r->post_parent;
}

/**
*	array_map callback to get `post_name` property of object
*	@param object
*	@return int
*/
function post_titles( $r ){
	return (string) $r->post_title;
}

/**
*	array_map callback to get `slug` property of object
*	@param object
*	@return string
*/
function slugs( $r ){
	return (string) $r->slug;
}

/**
*	array_map callback to get `term_id` property of object
*	@param object
*	@return int
*/
function term_ids( $r ){
	return (int) $r->term_id;
}

/**
*	array_map callback to get `term_taxonomy_id` property of object
*	@param object
*	@return int
*/
function term_taxonomy_ids( $r ){
	return (int) $r->term_taxonomy_id;
}
