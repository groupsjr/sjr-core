<div class="wrap">
	<div id="icon-themes" class="icon32"></div>

	<h2>SJR Core</h2>
    <pre><?php echo $version; ?></pre>
    
	<?php settings_errors(); ?>

	<h2 class="nav-tab-wrapper">
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_dev')) ); ?>" class="nav-tab <?php echo $active_tab == 'dev' ? 'nav-tab-active' : ''; ?>">Dev</a>
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_images')) ); ?>" class="nav-tab <?php echo $active_tab == 'images' ? 'nav-tab-active' : ''; ?>">Images</a>
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_licenses')) ); ?>" class="nav-tab <?php echo $active_tab == 'licenses' ? 'nav-tab-active' : ''; ?>">Licenses</a>
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_ordering')) ); ?>" class="nav-tab <?php echo $active_tab == 'ordering' ? 'nav-tab-active' : ''; ?>">Post Ordering</a>
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_site')) ); ?>" class="nav-tab <?php echo $active_tab == 'site' ? 'nav-tab-active' : ''; ?>">Site</a>
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_taxonomies')) ); ?>" class="nav-tab <?php echo $active_tab == 'taxonomies' ? 'nav-tab-active' : ''; ?>">Taxonomies</a>
        <a href="<?php echo esc_url( add_query_arg(array('page' => 'sjr-core_tools')) ); ?>" class="nav-tab <?php echo $active_tab == 'tools' ? 'nav-tab-active' : ''; ?>">Tools</a>

        <select name="sjr-env" id="sjr-env">
            <option <?php selected( 'db', $env ); ?> value="db">DB</option>
            <option <?php selected( 'local', $env ); ?> value="local">Local</option>
            <option <?php selected( 'stage', $env ); ?> value="stage">Stage</option>
            <option <?php selected( 'prod', $env ); ?> value="prod">Prod</option>
        </select>
    </h2>

	<form method="post" action="options.php" class="sjr-core">
         <input type="hidden" name="<?php echo $active_id; ?>[env]" value="<?php echo esc_attr( $env ); ?>"/>

        <?php

        settings_fields( $active_id );
        do_settings_sections( $active_id );
        
        submit_button();
        
        ?>
    </form>
</div>