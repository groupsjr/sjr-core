<div class="misc-pub-section sjr-fixed-order">
	<?php foreach( $page_types as $page_type ): ?>
	<label>
		Order in <?php echo $page_type->type; ?> pages
		<input type="text" class="" name="fixed-order[<?php echo $page_type->type; ?>]" value="<?php echo esc_attr( $page_type->value ); ?>" />
	</label>
<?php endforeach; ?>
</div>