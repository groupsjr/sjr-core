<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-dev-revisions-0" name="sjr-core_dev[revisions]" value="0" <?php checked( 0, $revisions ); ?>/>
		<label for="sjr-core-dev-revisions-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-dev-revisions-1" name="sjr-core_dev[revisions]" value="1" <?php checked( 1, $revisions ); ?>/>
		<label for="sjr-core-dev-revisions-1" class="on"></label>
	</div>
</fieldset>