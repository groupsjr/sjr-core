<input type="checkbox" class="autotoggle" id="placeholder" name="sjr-core_images[placeholder]" value="1" <?php checked( 1, $placeholder ); ?>/>
<label for="placeholder">Use <a href="https://placehold.it/" target="_blank">placehold.it</a> for missing images</label>

<div class="autotoggle <?php if( !$placeholder ) echo 'inactive'; ?>">
	<input type="checkbox" id="placeholder_force" <?php readonly( 0, $placeholder ); ?> name="sjr-core_images[placeholder_force]" value="1" <?php checked( 1, $placeholder_force ); ?>/>
	<label for="placeholder_force">Force placeholder</label>
</div>