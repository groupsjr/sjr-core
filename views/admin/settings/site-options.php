<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-serialized_options-0" name="sjr-core_site[serialized_options]" value="0" <?php checked( 0, $serialized_options ); ?>/>
		<label for="sjr-core-site-serialized_options-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-serialized_options-1" name="sjr-core_site[serialized_options]" value="1" <?php checked( 1, $serialized_options ); ?>/>
		<label for="sjr-core-site-serialized_options-1" class="on"></label>
	</div>

	<legend>
		Show serialized options in <a href="<?php echo admin_url( 'options.php' ); ?>">Settings</a>
	</legend>
</fieldset>