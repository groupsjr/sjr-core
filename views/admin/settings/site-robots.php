<fieldset>
	<div class="toggle-container">
		<input type="radio" class="autotoggle" id="sjr-core-site-robots-0" name="sjr-core_site[robots]" value="0" <?php checked( 0, $robots ); ?>/>
		<label for="sjr-core-site-robots-0" class="off"></label>

		<input type="radio" class="autotoggle" id="sjr-core-site-robots-1" name="sjr-core_site[robots]" value="1" <?php checked( 1, $robots ); ?>/>
		<label for="sjr-core-site-robots-1" class="on"></label>
	</div>

	<legend>
		Block robots based on hostname
	
		<div class="autotoggle <?php if( !$robots ) echo 'inactive'; ?>">
			<textarea <?php readonly( 0, $robots ); ?> class="large" name="sjr-core_site[robots_hosts]"><?php echo esc_textarea( $robots_hosts ); ?></textarea>
			<pre>hostnames contain</pre>
		</div>
	</legend>
</fieldset>