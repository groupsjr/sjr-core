<?php foreach( $taxonomies as $taxonomy ): ?>
	<label>
		<input type="checkbox" name="sjr-core_taxonomies[single][<?php echo $taxonomy->name; ?>]" value="1" <?php checked( $options->single[$taxonomy->name], 1 ); ?>/>
		
		<?php echo $taxonomy->labels->name; ?>
	</label>
<?php endforeach; ?>