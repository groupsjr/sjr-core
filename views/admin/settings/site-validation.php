<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-dom_errors-0" name="sjr-core_site[dom_errors]" value="0" <?php checked( 0, $dom_errors ); ?>/>
		<label for="sjr-core-site-dom_errors-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-dom_errors-1" name="sjr-core_site[dom_errors]" value="1" <?php checked( 1, $dom_errors ); ?>/>
		<label for="sjr-core-site-dom_errors-1" class="on"></label>
	</div>

	<legend>
		Show libxml errors
	</legend>
</fieldset>

<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-validate_content-0" name="sjr-core_site[validate_content]" value="0" <?php checked( 0, $validate_content ); ?>/>
		<label for="sjr-core-site-validate_content-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-validate_content-1" name="sjr-core_site[validate_content]" value="1" <?php checked( 1, $validate_content ); ?>/>
		<label for="sjr-core-site-validate_content-1" class="on"></label>
	</div>

	<legend>
		Validate HTML on post save
	</legend>
</fieldset>