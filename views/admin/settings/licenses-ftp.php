<div class="">
	<label>
		<span>Username</span>
		<input name="sjr-core_licenses[<?php echo $id; ?>][username]" type="text" class="small" value="<?php echo esc_attr( $creds['username'] ); ?>"/>
	</label>

	<br/>

	<label>
		<span>Password</span>
		<input name="sjr-core_licenses[<?php echo $id; ?>][password]" type="text" class="small" value="<?php echo esc_attr( $creds['password'] ); ?>"/>
	</label>

	<br/>

	<label>
		<span>Host</span>
		<input name="sjr-core_licenses[<?php echo $id; ?>][hostname]" type="text" class="medium" value="<?php echo esc_attr( $creds['hostname'] ); ?>"/>
	</label>
</div>