<input type="checkbox" class="autotoggle" id="offsite" name="sjr-core_images[offsite]" value="1" <?php checked( 1, $offsite ); ?>/>
<label for="offsite">Copy offsite images in post content to wp-content/uploads</label>

<div class="autotoggle <?php if( !$offsite ) echo 'inactive'; ?>">
	<textarea <?php readonly( 0, $offsite ); ?> class="large" name="sjr-core_images[offsite_include]"><?php echo esc_textarea( $offsite_include ); ?></textarea>
	<pre>Only copy from hosts - do not include http(s)://</pre>
</div>