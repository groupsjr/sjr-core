<fieldset>
	<div class="toggle-container">
		<input type="radio" class="autotoggle" id="sjr-core-ordering-fixed-0" name="sjr-core_ordering[fixed][on]" value="0" <?php checked( 0, $fixed['on'] ); ?>/>
		<label for="sjr-core-ordering-fixed-0" class="off"></label>

		<input type="radio" class="autotoggle" id="sjr-core-ordering-fixed-1" name="sjr-core_ordering[fixed][on]" value="1" <?php checked( 1, $fixed['on'] ); ?>/>
		<label for="sjr-core-ordering-fixed-1" class="on"></label>
	</div>

	<legend>
		Enable

		<div class="autotoggle <?php if( !$fixed['on'] ) echo 'inactive'; ?>">
			<?php foreach( $page_types as $page_type ): ?>
				<label>
					<input <?php readonly( 0, $fixed['on'] ); ?> <?php checked( 1, $fixed[$page_type] ); ?> name="sjr-core_ordering[fixed][<?php echo $page_type; ?>]" type="checkbox" class="" value="1"/>
					<?php echo ucfirst( $page_type ); ?> pages
				</label>
			<?php endforeach; ?>
		</div>
	</legend>
</fieldset>