<div class="">
	<label>
		<span>Access Token</span>
		<input name="sjr-core_licenses[bitly_access_token]" type="text" class="medium" value="<?php echo esc_attr( $bitly_access_token ); ?>"/>
		
	</label>

	<pre class="divider">or</pre>

	<label>
		<span>Login</span>
		<input name="sjr-core_licenses[bitly_login]" type="text" class="small" value="<?php echo esc_attr( $bitly_login ); ?>"/>
	</label>

	<br/>

	<label>
		<span>API Key</span>
		<input name="sjr-core_licenses[bitly_api_key]" type="text" class="medium" value="<?php echo esc_attr( $bitly_api_key ); ?>"/>
	</label>
</div>