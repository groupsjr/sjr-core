<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-adminbar-0" name="sjr-core_site[adminbar]" value="0" <?php checked( 0, $adminbar ); ?>/>
		<label for="sjr-core-site-adminbar-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-adminbar-1" name="sjr-core_site[adminbar]" value="1" <?php checked( 1, $adminbar ); ?>/>
		<label for="sjr-core-site-adminbar-1" class="on"></label>
	</div>

	<legend>
		Show Admin Bar on frontend
	</legend>
</fieldset>
