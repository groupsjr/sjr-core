<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-adminajax-0" name="sjr-core_site[adminajax]" value="0" <?php checked( 0, $adminajax ); ?>/>
		<label for="sjr-core-site-adminajax-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-adminajax-1" name="sjr-core_site[adminajax]" value="1" <?php checked( 1, $adminajax ); ?>/>
		<label for="sjr-core-site-adminajax-1" class="on"></label>
	</div>

	<legend>
		Rewrite wp-admin/admin-ajax.php to /ajax/
	</legend>
</fieldset>