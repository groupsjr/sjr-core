<fieldset>
	<div class="toggle-container">
		<input type="radio" class="autotoggle" id="sjr-core-site-dashboard_items-0" name="sjr-core_site[dashboard_items][on]" value="0" <?php checked( 0, $dashboard_items['on'] ); ?>/>
		<label for="sjr-core-site-dashboard_items-0" class="off"></label>

		<input type="radio" class="autotoggle" id="sjr-core-site-dashboard_items-1" name="sjr-core_site[dashboard_items][on]" value="1" <?php checked( 1, $dashboard_items['on'] ); ?>/>
		<label for="sjr-core-site-dashboard_items-1" class="on"></label>
	</div>

	<legend>
		Remove items from Admin Dashboard

		<div class="autotoggle <?php if( !$dashboard_items['on'] ) echo 'inactive'; ?>">
			<div class="toggle-container">
				<input type="radio" class="" id="sjr-core-site-dashboard_items-welcome-0" name="sjr-core_site[dashboard_items][welcome_panel]" value="0" <?php checked( 0, $dashboard_items['welcome_panel'] ); ?>/>
				<label for="sjr-core-site-dashboard_items-welcome-0" class="off"></label>

				<input type="radio" class="" id="sjr-core-site-dashboard_items-welcome-1" name="sjr-core_site[dashboard_items][welcome_panel]" value="1" <?php checked( 1, $dashboard_items['welcome_panel'] ); ?>/>
				<label for="sjr-core-site-dashboard_items-welcome-1" class="on"></label>
			</div>

			<legend>Welcome</legend>
		</div>
	</legend>
</fieldset>