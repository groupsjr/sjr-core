<fieldset>
	<div class="toggle-container">
		<input type="radio" class="autotoggle" id="sjr-core-site-posts_per_page-0" name="sjr-core_site[posts_per_page][on]" value="0" <?php checked( 0, $posts_per_page['on'] ); ?>/>
		<label for="sjr-core-site-posts_per_page-0" class="off"></label>

		<input type="radio" class="autotoggle" id="sjr-core-site-posts_per_page-1" name="sjr-core_site[posts_per_page][on]" value="1" <?php checked( 1, $posts_per_page['on'] ); ?>/>
		<label for="sjr-core-site-posts_per_page-1" class="on"></label>
	</div>

	<legend>
		Enabled
	
		<div class="autotoggle <?php if( !$posts_per_page['on'] ) echo 'inactive'; ?>">
			<input type="text" class="tiny" id="posts_per_page_is_home" name="sjr-core_site[posts_per_page][is_home]" value="<?php echo esc_attr( $posts_per_page['is_home'] ); ?>" />
			<label for="posts_per_page_is_home">Home</label>

			<br/>

			<input type="text" class="tiny" id="posts_per_page_is_category" name="sjr-core_site[posts_per_page][is_category]" value="<?php echo esc_attr( $posts_per_page['is_category'] ); ?>" />
			<label for="posts_per_page_is_category">Category</label>

			<br/>

			<input type="text" class="tiny" id="posts_per_page_is_search" name="sjr-core_site[posts_per_page][is_search]" value="<?php echo esc_attr( $posts_per_page['is_search'] ); ?>" />
			<label for="posts_per_page_is_search">Search</label>

			<br/>
			
			<input type="text" class="tiny" id="posts_per_page_is_tag" name="sjr-core_site[posts_per_page][is_tag]" value="<?php echo esc_attr( $posts_per_page['is_tag'] ); ?>" />
			<label for="posts_per_page_is_tag">Tag</label>
		</div>
	</legend>
</fieldset>