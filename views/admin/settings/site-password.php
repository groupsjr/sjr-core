<fieldset>
	<div class="toggle-container">
		<input type="radio" class="autotoggle" id="sjr-core-site-password-0" name="sjr-core_site[password][on]" value="0" <?php checked( 0, $password['on'] ); ?>/>
		<label for="sjr-core-site-password-0" class="off"></label>

		<input type="radio" class="autotoggle" id="sjr-core-site-password-1" name="sjr-core_site[password][on]" value="1" <?php checked( 1, $password['on'] ); ?>/>
		<label for="sjr-core-site-password-1" class="on"></label>
	</div>

	<legend>
		Enable 401 password protection

		<div class="autotoggle <?php if( !$password['on'] ) echo 'inactive'; ?>">
			<input class="medium" id="realm" name="sjr-core_site[password][realm]" value="<?php echo esc_attr( $password['realm'] ); ?>" placeholder="Realm"/>
			
			<?php foreach( $password['users'] as $k => $creds ): ?>
			<div class="add_user_item">
				<input placeholder="username" class="small" name="sjr-core_site[password][users][<?php echo $k; ?>][user]" type="text" value="<?php echo esc_attr( $creds['user'] ); ?>"/>
				<input placeholder="password" class="small" name="sjr-core_site[password][users][<?php echo $k; ?>][pass]" type="text" value="<?php echo esc_attr( $creds['pass'] ); ?>"/>

				<a href="" class="remove" title="Remove"><i class="fa fa-minus-square-o"></i></a>
			</div>
			<?php endforeach; ?>

			<button type="button" id="add_user">Add user <i class="fa fa-plus-square-o"></i></button>
		</div>
	</legend>

	

</fieldset>