<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-ordering-inline_sticky-0" name="sjr-core_site[inline_sticky]" value="0" <?php checked( 0, $inline_sticky ); ?>/>
		<label for="sjr-core-ordering-inline_sticky-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-ordering-inline_sticky-1" name="sjr-core_site[inline_sticky]" value="1" <?php checked( 1, $inline_sticky ); ?>/>
		<label for="sjr-core-ordering-inline_sticky-1" class="on"></label>
	</div>
</fieldset>