<input type="checkbox" class="" id="query_string" name="sjr-core_images[query_string]" value="1" <?php checked( 1, $query_string ); ?>/>
<label for="query_string">Add query string based on image meta to break caching</label>

<br/>

<input type="checkbox" class="" id="query_string_random" name="sjr-core_images[query_string_random]" value="1" <?php checked( 1, $query_string_random ); ?>/>
<label for="query_string_random">Add random query string to break caching</label>