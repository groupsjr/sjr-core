<div class="">
	<label>
		<span>API Key</span>
		<input name="sjr-core_licenses[ithemes_sec][api_key]" type="text" class="small" value="<?php echo esc_attr( $ithemes_sec['api_key'] ); ?>"/>
	</label>

	<br/>

	<label>
		<span>API Secret</span>
		<input name="sjr-core_licenses[ithemes_sec][api_secret]" type="text" class="large" value="<?php echo esc_attr( $ithemes_sec['api_secret'] ); ?>"/>
	</label>
</div>