<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-transients-0" name="sjr-core_site[transients]" value="0" <?php checked( 0, $transients ); ?>/>
		<label for="sjr-core-site-transients-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-transients-1" name="sjr-core_site[transients]" value="1" <?php checked( 1, $transients ); ?>/>
		<label for="sjr-core-site-transients-1" class="on"></label>
	</div>

	<legend>
		Use SJR Transients table (experimental)
	</legend>
</fieldset>