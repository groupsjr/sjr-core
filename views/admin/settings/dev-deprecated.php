<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-dev-deprecated_die-0" name="sjr-core_dev[deprecated_die]" value="0" <?php checked( 0, $deprecated_die ); ?>/>
		<label for="sjr-core-dev-deprecated_die-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-dev-deprecated_die-1" name="sjr-core_dev[deprecated_die]" value="1" <?php checked( 1, $deprecated_die ); ?>/>
		<label for="sjr-core-dev-deprecated_die-1" class="on"></label>
	</div>
</fieldset>