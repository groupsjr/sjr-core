<button type="submit" name="sjr-fix-images" value="<?php echo wp_create_nonce( 'sjr-fix-images' ); ?>">Run</button>
<input type="range" name="sjr-fix-images-count" min="1" value="10" max="1000" oninput="document.querySelector('#sjr-fix-images-count').value = value;" />
<output for="sjr-fix-images-count" id="sjr-fix-images-count">10</output>

<p>Last Posts fixed:</p>

<ol>
<?php foreach( $posts as $post ): ?>
	<li>
		<?php echo esc_html( $post->post_title ); ?>

		<?php edit_post_link( 'Edit', '', '', $post->ID ); ?>

		<?php foreach( $post->replacements as $replacement ): ?>
			<p>
				<span class="code"><?php echo $replacement['original']; ?></span>
				replaced with: 
				<span class="code"><?php echo $replacement['new']; ?></span>
			</p>
		<?php endforeach; ?>
	</li>
<?php endforeach; ?>
</ol>