<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-async-0" name="sjr-core_site[async]" value="0" <?php checked( 0, $async ); ?>/>
		<label for="sjr-core-site-async-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-async-1" name="sjr-core_site[async]" value="1" <?php checked( 1, $async ); ?>/>
		<label for="sjr-core-site-async-1" class="on"></label>
	</div>

	<legend>
		Use #async and #defer in script src
	</legend>
</fieldset>