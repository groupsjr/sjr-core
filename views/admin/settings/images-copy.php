<input type="checkbox" class="autotoggle" id="copy_images" name="sjr-core_images[copy_images]" value="1" <?php checked( 1, $copy_images ); ?>/>
<label for="copy_images">Define environments to copy missing images</label>

<div class="autotoggle <?php if( !$copy_images ) echo 'inactive'; ?>">
	<textarea <?php readonly( 0, $copy_images ); ?> class="large" name="sjr-core_images[copy_images_environments]"><?php echo esc_textarea( $copy_images_environments ); ?></textarea>
	<pre>URL to uploads directory ex: <?php echo $baseurl; ?></pre>
</div>

<div class="autotoggle">
	<input id="s3_copy_replace" type="checkbox" name="sjr-core_images[s3_copy_replace]" value="1" <?php checked( 1, $s3_copy_replace ); ?>/>
	<label for="s3_copy_replace">Copy to s3 and content replace</label>
</div>

<div class="autotoggle <?php if( !$copy_images ) echo 'inactive'; ?>">
	<input type="checkbox" class="autotoggle" id="copy_images_debug" name="sjr-core_images[copy_images_debug]" value="1" <?php checked( 1, $copy_images_debug ); ?>/>
	debug
</div>