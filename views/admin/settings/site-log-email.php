<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-log_email-0" name="sjr-core_site[log_email]" value="0" <?php checked( 0, $log_email ); ?>/>
		<label for="sjr-core-site-log_email-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-log_email-1" name="sjr-core_site[log_email]" value="1" <?php checked( 1, $log_email ); ?>/>
		<label for="sjr-core-site-log_email-1" class="on"></label>
	</div>

	<legend>
		Log mail
	</legend>
</fieldset>