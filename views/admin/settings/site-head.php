<fieldset>
	<div class="toggle-container">
		<input type="radio" class="" id="sjr-core-site-head-0" name="sjr-core_site[head]" value="0" <?php checked( 0, $head ); ?>/>
		<label for="sjr-core-site-head-0" class="off"></label>

		<input type="radio" class="" id="sjr-core-site-head-1" name="sjr-core_site[head]" value="1" <?php checked( 1, $head ); ?>/>
		<label for="sjr-core-site-head-1" class="on"></label>
	</div>

	<legend>
		Remove <code>rsd_link</code>, <code>wlwmanifest_link</code>, and <code>wp_generator</code> from head element
	</legend>
</fieldset>