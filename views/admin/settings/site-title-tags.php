

<fieldset>
	<div class="toggle-container">
		<input type="radio" class="autotoggle" id="sjr-core-site-title_tags-0" name="sjr-core_site[title_tags][on]" value="0" <?php checked( 0, $title_tags['on'] ); ?>/>
		<label for="sjr-core-site-title_tags-0" class="off"></label>

		<input type="radio" class="autotoggle" id="sjr-core-site-title_tags-1" name="sjr-core_site[title_tags][on]" value="1" <?php checked( 1, $title_tags['on'] ); ?>/>
		<label for="sjr-core-site-title_tags-1" class="on"></label>
	</div>

	<legend>
		Enable title tag filtering
	
		<div class="autotoggle <?php if( !$title_tags['on'] ) echo 'inactive'; ?>">
			<textarea class="large code yaml" id="realm" name="sjr-core_site[title_tags][config]" placeholder=""><?php echo esc_textarea( $title_tags['config'] ); ?></textarea>

			<br/>

			<div class="toggle-container">
				<input type="radio" class="" id="sjr-core-site-title_tags-remove-0" name="sjr-core_site[title_tags][remove]" value="0" <?php checked( 0, $title_tags['remove'] ); ?>/>
				<label for="sjr-core-site-title_tags-remove-0" class="off"></label>

				<input type="radio" class="" id="sjr-core-site-title_tags-remove-1" name="sjr-core_site[title_tags][remove]" value="1" <?php checked( 1, $title_tags['remove'] ); ?>/>
				<label for="sjr-core-site-title_tags-remove-1" class="on"></label>
			</div>

			<legend>Remove tag content</legend>
		</div>
		</legend>
</fieldset>