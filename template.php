<?php

namespace sjr;

/**
*
*/
function enqueue_font_awesome(){
	wp_enqueue_style( 'sjr-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', 
                       array(), '' );
}

/**
*	force protocol relative urls for js and css assets
*	@param string
*	@param string
*	@param string
*	@return string
*/
function loader_src( $src, $handle ){
	if( strpos($src, 'http://') === 0 )
		$src = substr( $src, 5 );
	
	return $src;
}
add_filter( 'script_loader_src', __NAMESPACE__.'\loader_src', PHP_INT_MAX - 1, 3 );
add_filter( 'style_loader_src', __NAMESPACE__.'\loader_src', PHP_INT_MAX - 1, 3 );

/**
*	attached to `print_head_scripts` and `print_footer_scripts` actions
*	@param bool
*	@return bool
*/
function print_scripts_async( $true ){
	global $wp_scripts;
	
	$html = &$wp_scripts->print_html;
	$html = preg_replace_callback( '/#(async|defer)\'/', __NAMESPACE__.'\print_scripts_callback', $html );

	return $true;
}

/**
*	preg_replace callback for print_scripts_async
*	@param array
*	@return string
*/
function print_scripts_callback( $args ){
	return "' {$args[1]} ";
}

/**
*	for debugging template used
*	@param string
*	@return string
*/
function template_include( $template ){
	return $template;
}
add_filter( 'template_include', __NAMESPACE__.'\template_include', PHP_INT_MAX - 1 );