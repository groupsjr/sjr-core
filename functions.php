<?php

namespace sjr;

use Symfony\Component\Yaml;

/**
*	
*	@param
*	@param
*	@param
*	@return string;
*/
function active( $a, $b, $display = 'active' ){
	if( strcasecmp($a, $b) === 0 )
		return $display;

	return '';
}

/**
*
*	@param string absolute url of image
*	@return string file path in /yyyy/mm/file.jpg format
*/
function attachment_path( $url ){
	$wp_upload_dir = wp_upload_dir();
	$baseurl = $wp_upload_dir['baseurl'];
	
	if( stripos($url, $baseurl) !== FALSE ){
		$file_path = substr( $url, stripos($url, $baseurl) + strlen($baseurl) );
	} elseif( strpos($url, '/wp-content/uploads') !== FALSE ){
		// @TODO make this work with non standard paths
		$file_path = substr( $url, strpos($url, '/wp-content/uploads') + strlen('/wp-content/uploads') );
	} else {
		$file_path = FALSE;
	}

	return $file_path;
}

/**
*	takes an attachments url and finds the absolute local path
*	@param string url of image on current server
*	@return string absolute path of equivalent image on current server
*/
function attachment_url_to_path( $url ){
	// $file_path is in /yyyy/mm/file.jpg format
	$file_path = attachment_path( $url );
	if( empty($file_path) ){
		return FALSE;
	}

	$path = pathinfo( $file_path );

	preg_match( '/\/([0-9]*\/[0-9]*)/', $path['dirname'], $upload_dir ); 

	if( empty($upload_dir[1]) ){
		$wp_upload_dir = wp_upload_dir();
	} else {
		// $upload_dir[1] is yyyy/mm
		$wp_upload_dir = wp_upload_dir( $upload_dir[1] );
	}

	$local_file = $wp_upload_dir['basedir'].$file_path;

	return $local_file;
}

/**
*	uses lib/
*	@param string
*	@return
*/
function autoload( $class ){ 
	$file = str_replace( '\\', '/', strtolower($class) );
	$file = str_replace( '_', '-', $file );

	$file = __DIR__.'/lib/'.$file.'.php';
	
	if( file_exists($file) ){
		$ok = require_once $file;
	}
}
spl_autoload_register( __NAMESPACE__.'\autoload' );

/**
*
*	@return string
*/
function blank_gif(){
	return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
}

/**
*	gets a file from the other envirnoments and copies if it exists
*	@param string url on the current server of file that is missing 
*	@param string absolute path on current server file will be copied to
*	@return int number of bytes in file copied | FALSE
*/
function copy_file_from_url( $url, $local_file ){
	// @TODO make the time limit configurable
	if( Timer::instance()->seconds_elapsed() > 5 )
		return 0;

	$dbug = get_sjr_option( 'images', 'copy_images_debug' ) && function_exists( 'dbug' );
	$options = get_sjr_option( 'images' );

	$wp_upload_dir = wp_upload_dir();
	$target = dirname( $local_file );
	
	if( !is_dir($target) ){
		wp_mkdir_p( $target );
	}

	if( !is_dir($target) ){
		$return = FALSE;
		$dbug_message = "the local directory $target could not be created";
	} elseif( !is_writable($target) ){
		$return = FALSE;
		$dbug_message = "the local directory $target is not writable";
	} elseif( $image = get_file_from_url($url) ){
		$return = file_put_contents( $local_file, $image->file_contents );
		$dbug_message = "{$image->remote_file} copied to $local_file";
	} else {
		$return = FALSE;
		$dbug_message = "$url was not able to be copied from the defined environments.";
	}

	if( $dbug )
		dbug( $return, $dbug_message, 10 );

	return $return;
}

/**
*	check if table exists, drop or create if user requests
*	@param bool
*	@return
*/
function transients_db_check( $on = FALSE ){
	global $wpdb;

	$table_name = $wpdb->prefix.'sjr_transients';

	$sql = $wpdb->prepare( "SHOW TABLES LIKE %s", $table_name );
	$exists = $wpdb->get_var( $sql );

	if( $on && !$exists ){
		transients_db_create();
	} elseif( !$on && $exists ){
		transients_db_delete();
	}
}

/**
*	create db table for `SJR Transients`
*	@return
*/
function transients_db_create(){
	global $wpdb;
	require_once ABSPATH.'/wp-admin/includes/upgrade.php';

	$table_name = $wpdb->prefix.'sjr_transients';

	$sql = "CREATE TABLE `$table_name` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`key` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
				`data` longtext CHARACTER SET utf8mb4,
				`expire` int(11) DEFAULT NULL,
				`group` varchar(16) CHARACTER SET utf8mb4 DEFAULT '',
				PRIMARY KEY (`id`),
				KEY `key` (`key`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
	$ok = dbDelta( $sql );
	
}

/**
*	delete db table for `SJR Transients`
*	@return bool
*/
function transients_db_delete(){
	global $wpdb;

	$table_name = $wpdb->prefix.'sjr_transients';
	$sql = "DROP table `$table_name`";

	$ok = $wpdb->query( $sql );
	return $ok;
}

/**
*
*	@return
*/
function transients_db_check_obj_cache(){
	if( !file_exists(WP_CONTENT_DIR . '/object-cache.php') ){
		$cache_php = file_get_contents( SJR_CORE_DIR.'/cache.php' );
		$cache_php = str_replace( "__DIR__.'/", "'".SJR_CORE_DIR."/", $cache_php );

		file_put_contents( WP_CONTENT_DIR . '/object-cache.php', $cache_php );
	} else {
		
	}

	return;
}

/**
*
*	
*	@return int rows deleted | TRUE
*/
function delete_transients(){
	global $wp_object_cache, $wpdb;

	if( wp_using_ext_object_cache() ){
		// roundabout way of setting private $cache properties with magic __set in WP_Object_Cache
		$cache = $wp_object_cache->cache;
		$cache = array_merge( $cache, array(
			'transient' => array()
		) );
		
		$wp_object_cache->cache = $cache;
		return TRUE;
	} else {
		$sql = "DELETE FROM $wpdb->options
				WHERE option_name LIKE '_transient_%'
				OR option_name LIKE '_site_transient_%'";
		$res = $wpdb->query( $sql );

		return $res;
	}	
}

/**
*	remove `Uncategorized`
*	@return 0 | TRUE
*/
function delete_uncategorized(){
	$ok = wp_delete_term( 1, 'category' );
	if( $ok )
		delete_option( 'default_category' );

	return $ok;
}

/**
*	takes a local url of a missing image, iterates through possible locations in other environments, 
*	and returns the file contents
*	@param string 
*	@return object with `file_contents` and `remote_file` keys or FALSE
*/
function get_file_from_url( $url ){
	$options = get_sjr_option( 'images' );
	$file_path = attachment_path( $url );

	$image = FALSE;

	// support multiple envs
	$envs = array_filter( array_map(function($env) use ($file_path){
		return trim($env) ? $env.$file_path : NULL;
	}, $options->copy_images_environments) );

	foreach( $envs as $remote_file ){
		if( !$image ){
			
			$response = wp_remote_get( $remote_file, array(
				//'blocking' => FALSE,
				'timeout' => 60
			) );

			if( !is_wp_error($response) && $response['response']['code'] === 200 ){
				$image = (object) array(
					'file_contents' => wp_remote_retrieve_body( $response ),
					'remote_file' => $remote_file
				);

				break;
			}
		}
	}

	return $image;
}

/**
*	gets the width, height, and crop for a registered image size
*	@param string | array
*	@return array
*/
function get_image_dims( $size ){
	global $_wp_additional_image_sizes;

	if( is_string($size) && isset($_wp_additional_image_sizes[$size]) ){
		$dims = $_wp_additional_image_sizes[$size];
	} elseif( is_string($size) && in_array($size, get_intermediate_image_sizes()) ){
        $dims = array(
			'width' => get_option( $size . '_size_w' ),
			'height' => get_option( $size . '_size_h' ),
			'crop' => (bool) get_option( $size . '_crop' )
		);
	} elseif( is_string($size) && ($size == 'full') ){
		// @TODO make a configurable size for default full size images
		$dims = array(
			'width' => 2048,	//
			'height' => 1024,  //
			'crop' => TRUE  //
		);
	} elseif( is_array($size) ){
		$dims = array(
			'width' => $size[0],	//
			'height' => $size[1],  //
			'crop' => TRUE  //
		);
	} else {
		$dims = array(
			'width' => 1,	//
			'height' => 1,  //
			'crop' => TRUE  //
		);
	}

	return $dims;
}

/**
*
*	@param int
*	@param int
*	@return string
*/
function get_placeholder_image( $width = 150, $height = 150 ){
	$height = max( 1, $height );
	$width = max( 1, $width );

	$placeholder = sprintf( 'https://placehold.it/%dx%d', $width, $height );
	
	return $placeholder;
}

/**
*
*	@param string 'dev', 'images', etc
*	@return array
*/
function get_sjr_defaults( $option_name ){
	switch( $option_name ){
		case 'dev':
			$defaults = array(
				'deprecated_die' => 0,
				'revisions' => 0
			);
			break;

		case 'images':
			$defaults = array(
		        'copy_images' => 0,
		        'copy_images_debug' => 0,
		        'copy_images_environments' => '',		// formatted later to array
		        'offsite' => 0,
		        'offsite_include' => 'i.imgur.com',		// formatted later to array
		        'placeholder' => 0,
		        'placeholder_force' => 0,
		        'query_string' => 0,
		        'query_string_random' => 0,
		        's3_copy_replace' => 0
		    );
			break;

		case 'licenses':
			$defaults = array(
				'acf_pro' => 'b3JkZXJfaWQ9NTUyNzh8dHlwZT1wZXJzb25hbHxkYXRlPTIwMTUtMDUtMDEgMTY6MDI6NTE=',
				'bitly_access_token' => '',
				'bitly_api_key' => '',
				'bitly_login' => '',
				'ftp' => array(
					'hostname' => '',
					'password' => '',
					'username' => '',
				),
				'ithemes_sec' => array(
					'api_key' => '7Xpq9lX7GuF2Hz07hSPd8DW09vz6ARnZ',
					'api_secret' => 'M97X3sG6p1G4qQl9oGqdWdUN0T2urm03cDl74D5sl2p0eTI7GkWImW9hH91IIQ6s4IDkIv6VvLwDF67B267RLZB51a6eYemgzH0WqlD8vy8xoh60jgCW8TaBEOfJt3Zn'
				),
				'offload_s3' => '9b4cdaf3-5dd0-4bad-98d8-245a89b4753b',
				'yandex' => 'trnsl.1.1.20161110T190532Z.3840ae7d85fbecd2.162ae782906af2c091072e342242f7826d17d094',
				'wordpress_api_key' => '5dcec51b0572',
				'wpml' => '',
				'wpmudev' => 'adfb71bafd47a16d0138ecdc4aaeb0ae33d3e8dc'
			);
			break;

		case 'ordering':
			$defaults = array(
				'fixed' => array(
					'on' => 0,
					'archive' => 0,
					'category' => 0,
					'home' => 0,
					'search' => 0,
					'tag' => 0
				),
				'inline_sticky' => 0
			);
			break;

		case 'site':
			$defaults = array(
				'adminajax' => 0,
				'adminbar' => 1,
				'async' => 0,
				'dashboard_items' => array(
					'on' => 0,
					'welcome_panel' => 0
				),
				'dom_errors' => 0,
				'head' => 1,
				'log_email' => 0,
				'password' => array(
					'on' => 0,
					'realm' => 'Restricted Area',
					'users' => array(
						
					)
				),
				'posts_per_page' => array(
					'on' => 0,
					'is_category' => 10,
					'is_home' => 9,
					'is_search' => 12,
					'is_tag' => 9
				),
				'robots' => 0,
				'robots_hosts' => "dev.\n.devsjr.com", 	// formatted later to array
				'serialized_options' => 0,
				'title_tags' => array(
					'config' => '',
					'on' => 0,
					'remove' => 0
				),
				'transients' => 0,
				'validate_content' => 0
			);
			break;

		case 'taxonomies':
			$taxonomies = get_taxonomies( array(
		        'show_ui' => TRUE
		    ) );

		    $single = array_combine( 
		    	array_keys($taxonomies), 
		    	array_fill(0, count($taxonomies), 0) 
		    );
			$defaults = array(
				'single' => $single
			);
			break;

		case 'tools':
			$defaults = array();
			break;
			
		/*
		case '':
			$defaults = array();
			break;
		*/

		default:
			if( WP_DEBUG && function_exists('dbug') )
				dbug( $option_name, 'no defaults', 100 );

			$defaults = array();
			break;
	}

	return $defaults;
}

/**
*
*	@param string 'dev', 'images' etc
*	@param string specific option
*	@return object
*/
function get_sjr_option( $option_name, $setting = '' ){
	static $all;
	if( !$all )
		$all = array();

	if( !array_key_exists($option_name, $all) ){
		$name = 'sjr-core_'.$option_name;
		$option = (array) get_option( $name, array() );
		
		$defaults = get_sjr_defaults( $option_name );

		// set defaults recursively
		$option = array_merge( $defaults, $option );

		foreach( $defaults as $key => $value ){
			if( is_array($value) )
				$option[$key] = array_merge( $value, $option[$key] );
		}

		// extra formatting
		switch( $option_name ){
			case 'images':
				$option['copy_images_environments'] = explode( "\n", $option['copy_images_environments'] );
				$option['offsite_include'] = explode( "\n", $option['offsite_include'] );
				break;

			case 'site':
				$option['robots_hosts'] = explode( "\n", $option['robots_hosts'] );
				if( empty($option['password']['users']) )
					$option['password']['users'] = array( 0 => array('user' => '', 'pass' => '') );
				break;
		}

		$all[$option_name] = (object) $option;
	}

	if( $setting )
		return $all[$option_name]->$setting;
	else
		return $all[$option_name];
}

/**
*
*	@param
*	@return
*/
function get_domel_html( $el ){
	return $el->ownerDocument->saveHTML( $el );
}

/**
*	
*	@param DomElement
*	@return string
*/
function get_domel_xml( \DOMElement $el ){
	return $el->ownerDocument->saveXML( $el );
}

/**
*
*	@param DOMNodeList
*	@return string
*/
function get_domli_html( \DOMNodeList $li ){
	$html = '';

	foreach( $li as $el ){
		if( $el instanceof \DOMElement )
			$html .= get_domel_xml( $el );
		else
			$html .= get_domel_html( $el );
	}

	return $html;
}

/**
*
*	@param DOMNodeList
*	@return string
*/
function get_domli_html_inner( \DOMNodeList $li ){
	$html = '';

	foreach( $li as $el ){
		$html .= get_domli_html( $el->childNodes );
	}

	return $html;
}

/**
*	helper function to standardize saveHTML across libxml and php versions
*	@param DomDocument
*	@param DomElement
*	@return string html
*/
function get_html_body( \DomDocument $doc, $element = NULL ){
	// doctype tag
	if( !defined('LIBXML_HTML_NODEFDTD') && $doc->doctype )
		$doc->removeChild( $doc->doctype ); 

	$html = $doc->saveHTML( $element );

	// html and body tags
	// @TODO figure this out
	if( 1 || !defined('LIBXML_HTML_NOIMPLIED') ){
		// bad but it works 
		$html = preg_replace( '~<(?:!|/?(?:html|head|body))[^>]*>\s*~i', '', $html );
	}

	return $html;
}

/**
*	standardized method of getting DomDocument for html string
*	@param string html
*	@param string version number of the document as part of the XML declaration
*	@param string encoding of the document as part of the XML declaration
*	@param int loadHTML options
*	@return DOMDocument
*/
function get_html_dom( $html, $version = '1.0', $encoding = 'UTF-8' ){
	$doc = new \DOMDocument( $version, $encoding );

	// compat crazies
	if( $version == NULL ){
		// the doctype tag 
		$version = defined('LIBXML_HTML_NODEFDTD') ? ($version | constant('LIBXML_HTML_NODEFDTD')) : $version;

		// the html and body tag
		$version = defined('LIBXML_HTML_NOIMPLIED') ? ($version | constant('LIBXML_HTML_NOIMPLIED')) : $version;
	}

	// maybe?
	/*
	$doc->validateOnParse = TRUE;
	$doc->preserveWhiteSpace = FALSE;
	$doc->formatOutput = TRUE;
	*/

	// this is sometimes needed and showd be called before get_html_dom(), 
	// not tested enough to use all the time
	// $html = mb_convert_encoding( $html, 'HTML-ENTITIES', "UTF-8" );

	libxml_use_internal_errors( !get_sjr_option('site', 'dom_errors') );

	if( version_compare(phpversion(), '5.4', '>=') ){
		$doc->loadHTML( $html, $version );
	} else {
		$doc->loadHTML( $html );
	}

	$errors = libxml_get_errors();

	if( !empty($errors) && function_exists('dlog') ){
		foreach( $errors as $error ){
			dlog( (array)$error, $html, 'get_html_dom-errors' );
		}
	}

	return $doc;
}

/**
*	gets all of one tag from html
*	note - transients (serializing) does not work with DOMNodeList!
*	@param string | DomDocument
*	@param string
*	@return DOMNodeList
*/
function get_tags_from_html( $html, $tag = 'img' ){
	if( $html instanceof \DOMDocument ){
		$doc = $html;
	} elseif( is_string($html) && trim($html) ){
		$doc = get_html_dom( $html );
	} else {
		return new \DOMNodeList;
	}

	$tags = $doc->getElementsByTagName( $tag );

	return $tags;
}

/**
*	formats a string of text to php native
*	@param string YAML formatted
*	@return array | string
*/
function get_yaml( $yaml = '' ){
	if( is_array($yaml) ){
		return array_map( __NAMESPACE__.'\get_yaml', $yaml );
	}

	try{
        $yaml_to_php = Yaml\Yaml::parse( $yaml );

    	if( is_null($yaml_to_php) )
    		$yaml_to_php = '';
    } catch( Yaml\Exception\ParseException $e ){
        $yaml_to_php = '';
    }

    return $yaml_to_php;
}

/**
*
*	@param string
*/
function http_auth_send_401( $realm = 'Restricted area' ){
	$header = sprintf( 'WWW-Authenticate: Digest realm="%s",qop="auth",nonce="%s",opaque="%s"', $realm, uniqid(), md5($realm) );
	
	header( 'HTTP/1.1 401 Unauthorized');
	header( $header );
	die();
}

/**
*
*	@see https://secure.php.net/manual/en/features.http-auth.php
*	@param string
*	@return array | FALSE;
*/
function http_auth_digest_parse( $auth_digest ){
    // protect against missing data
    $needed_parts = array( 'nonce'=>1, 
    					   'nc'=>1, 
    					   'cnonce'=>1, 
    					   'qop'=>1, 
    					   'username'=>1, 
    					   'uri'=>1, 
    					   'response'=>1 );
    $data = array();
    $keys = implode( '|', array_keys($needed_parts) );

    preg_match_all( '@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $auth_digest, $matches, PREG_SET_ORDER );

    foreach( $matches as $m ){
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset( $needed_parts[$m[1]] );
    }

    return $needed_parts ? false : $data;
}

/**
*
*	@return bool
*/
function is_ajax(){
	return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

/**
*
*	@param WP_Post
*	@return bool
*/
function is_post_real_save( $wp_post ){
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return FALSE;

	if( defined('DOING_CRON') && DOING_CRON )
		return FALSE;

	if( wp_is_post_autosave($wp_post) )
		return FALSE;

	if( wp_is_post_revision($wp_post) )
		return FALSE;

	return TRUE;
}

/**
*	recusrsive ksort
*	@param array
*	@param int
*	@return bool
*/
function ksort_r( &$array, $sort_flags = SORT_REGULAR ){
    if( !is_array($array) )
    	return false;

    ksort( $array, $sort_flags );

    foreach( $array as &$arr ){
        ksort_r( $arr, $sort_flags );
    }

    return true;
}

/**
*	cast array to object
*	@param mixed
*	@return object
*/
function object( $array = NULL ){
	return (object) $array;
}

/**
*
*	@param
*	@param
*/
function parse_str( $query, &$return ){
	\parse_str( $query, $return );
	ksort_r( $return );
}

/**
*	imporved parse_url which returns the query string parsed into array
*	@param string
*	@param int
*	@return array
*/
function parse_url( $url, $component = -1 ){
	$parsed_url = \parse_url( $url, $component );

	if( $component === PHP_URL_QUERY )
		parse_str( $parsed_url, $parsed_url );
	elseif( !is_array($parsed_url) )
		$parsed_url;
	elseif( !isset($parsed_url['query']) )
		$parsed_url['query'] = array();
	else
		parse_str( $parsed_url['query'], $parsed_url['query'] );

	return $parsed_url;
}

/**
*	render a template file from /views/ with local variables
*	this function is copied throughout sjr/page.io 
*	custom plugins to standardize template rendering
*	@param string template name in /views/ without .php
*	@param object|array
*	@return string html
*/
function render( $template, $vars = array() ){
	return \sjr\render_theme_instance( __DIR__, __NAMESPACE__ )->process( $template, $vars );
}

/**
*
*	@param string
*	@param string
*	@return \sjr\Render
*/
function render_theme_instance( $root, $namespace ){
	static $plugins = array();

	if( empty($plugins[$namespace]) ){
		$plugins[$namespace] = new Render( $root );
	}

	return $plugins[$namespace];
}

/**
*	in progress
*	save serttings to file
*	@param array
*	@param
*	@param
*	@return array
*/
function save_env( $new_vals, $old_vals, $group ){
	$env = $new_vals['env'];
	if( !trim($env) )
		return $new_vals;

	unset( $new_vals['env'] );

	$config_file = SJR_CORE_DIR.'/config/'.$env.'.php';

	if( file_exists($config_file) )
		$config = file_get_contents( $config_file );
	else
		$config = '';

	$config = (array) json_decode( $config );
	$config[$group] = $new_vals;

	$config = json_encode( $config, JSON_PRETTY_PRINT );
	file_put_contents( $config_file, $config );

	

	return $new_vals;
}

/**
*	create default option values if they dont exist and register the setting
*	@param string 'dev', 'images' etc
*	@return
*/
function setup_sjr_defaults( $option_name ){
	$group = 'sjr-core_'.$option_name; // @TODO what should this be
	$name = 'sjr-core_'.$option_name;
	$sanitize = __NAMESPACE__.'\sanitize_'.$option_name;

	// $name is actual wp option name
	if( FALSE === get_option($name) ){
		$defaults = get_sjr_defaults( $option_name );
		add_option(
            $name, 
            apply_filters( $name.'-defaults', $defaults ) 
        );
	}
	
	add_filter( 'pre_update_option_'.$name, function($new_vals, $old_vals) use ($name){
		save_env( $new_vals, $old_vals, $name );
		return $new_vals;
	}, 10, 2 );
	
	return register_setting(
        $group,
        $name,
        $sanitize               // validation callback
    );
}

/**
*
*	http://php.net/manual/en/function.strip-tags.php#86964
*	@param string
*	@param string
*	@param bool
*	@return string
*/
function strip_tags_content( $text, $allowed_tags = '', $invert = FALSE ){
	preg_match_all( '/<(.+?)[\s]*\/?[\s]*>/si', trim($allowed_tags), $allowed_tags );
	$allowed_tags = array_unique( $allowed_tags[1] );

	if( is_array($allowed_tags) AND count($allowed_tags) > 0 ){
		if( $invert == FALSE ){
			return preg_replace( '@<(?!(?:'. implode('|', $allowed_tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text );
		} else {
			return preg_replace( '@<('. implode('|', $allowed_tags) .')\b.*?>.*?</\1>@si', '', $text );
		}
	} elseif( $invert == FALSE ){
		return preg_replace( '@<(\w+)\b.*?>.*?</\1>@si', '', $text );
	}

	return $text;
}

/**
*
*	@param string
*	@param array
*	@return string
*/
function transient_key( $prefix, array $vars ){
	$vars[] = version();

	$key = substr( $prefix.'-'.md5(serialize($vars)), 0, 40 );

	return $key;
}

/**
*	complement to parse_url which can also take a query as an array
*	@param array
*	@return string
*/
function unparse_url( $parsed_url ){
	$scheme = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
	$host = isset($parsed_url['host']) ? $parsed_url['host'] : '';
	$port = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';

	$user = isset($parsed_url['user']) ? $parsed_url['user'] : '';
	$pass = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
	$pass = ($user || $pass) ? "$pass@" : '';

	$path = isset($parsed_url['path']) ? $parsed_url['path'] : '';

	if( empty($parsed_url['query']) )
		$query = '';
	elseif( is_array($parsed_url['query']) )
		$query = '?' . http_build_query( $parsed_url['query'] );
	else
		$query = '?' . $parsed_url['query'];

	$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
	
	return trailingslashit( $scheme.$user.$pass.$host.$port ).ltrim( $path.$query.$fragment, '/' );
} 

/**
*	gets current version of plugin
*	@return string
*/
function version(){
	static $version;

	if( !$version ){
		$data = get_file_data( __DIR__.'/_plugin.php', array('Version'), 'plugin' );
		$version = $data[0];
	}
	
	return $version;
}

/**
*
*	@param array
*	@return string
*/
function wpdb_sanitize_in( $haystack ){
	global $wpdb;

	$in_quotes = array_map( function($s) use($wpdb){
		$specifier = is_int( $s ) ? '%d' : '%s';

		return $wpdb->prepare( $specifier, $s );
	}, $haystack );

	return $in_quotes ? implode( ', ', $in_quotes ) : "''";
}

/**
*	machine language translation
*	@param string html | array
*	@param string
*	@param string
*	@return string
*/
function yandex_translate( $source, $lang_from = 'en', $lang_to = 'de' ){
	$options = get_sjr_option( 'licenses' );

	if( is_string($source) ){
		$transient_key = transient_key( 'yandex_translate', array($source, $lang_from, $lang_to, version(), $options->yandex) );
		$html = get_transient( $transient_key );
		
		if( !$html ){
			$url = sprintf( 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s', $options->yandex );

			$response = wp_remote_get( $url, array(
				'body' => array(
					'text' => $source,
					'lang' => $lang_to,
					'format' => 'html'
				),
				'timeout' => 60
			) );

			if( !is_wp_error($response) ){
				$body = wp_remote_retrieve_body( $response );
				$json = json_decode( $body );

				if( !empty($json->text[0]) ){
					$html = $json->text[0];
				}
			}

			set_transient( $transient_key, $html, HOUR_IN_SECONDS );
			return $html;
		}
	} elseif( is_array($source) ){
		return array_map( function($item) use( $lang_from, $lang_to ){
			return yandex_translate( $item, $lang_from, $lang_to );
		}, $source );
	}
	
	return $source;
}