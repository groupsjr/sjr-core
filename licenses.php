<?php

namespace sjr;

/**
*	license key for offload pro
*	@param string
*	@return string
*/
function as3cf_setting_licence( $val ){
	$options = get_sjr_option( 'licenses' );
	if( !empty($options->offload_s3) ){
		$val = $options->offload_s3;
	}

	return $val;
}
add_filter( 'as3cf_setting_licence', __NAMESPACE__.'\as3cf_setting_licence' );

/**
*	wpml key
*	@todo implement
*	@param string
*	@return string
*/
function option_wpml_license( $val ){
	$_val = unserialize( gzuncompress(base64_decode($val)) );
	if( is_array($_val) ){
		$_val['site_key'] = '';	
		$_val['repositories']['wpml']['subscription'] = array(
			'data' => (object) array(
				'expires' => time() + DAY_IN_SECONDS,
				'status' => 1,
			),
			'key' => ''
		);

		$val = base64_encode(gzcompress(serialize($_val)));
	}

	return $val;
}
//add_filter( 'option_wp_installer_settings', __NAMESPACE__.'\option_wp_installer_settings', 10, 1 );

/**
*	license key for acf pro
*	@param
*	@return string
*/
function pre_option_acf_pro_license( $val ){
	$options = get_sjr_option( 'licenses' );

	if( !empty($options->acf_pro) ){
		$val = array(
			'key' => $options->acf_pro,
			'url' => home_url()
		);

		$val = base64_encode( serialize($val) );
	}

	return $val;
}
add_filter( 'pre_option_acf_pro_license', __NAMESPACE__.'\pre_option_acf_pro_license', 10, 1 );

/**
*	license for wordpress api
*	@param
*	@return string
*/
function pre_option_wordpress_api_key( $val ){
	$options = get_sjr_option( 'licenses' );
	
	if( !empty($options->wordpress_api_key) ){
		$val = $options->wordpress_api_key;
	}
	
	return $val;
}
add_filter( 'pre_option_wordpress_api_key', __NAMESPACE__.'\pre_option_wordpress_api_key', 10, 1 );

/**
*	license for wpmu dev / wp smush
*	@param
*	@return string
*/
function pre_option_wpmudev_apikey( $val ){
	$options = get_sjr_option( 'licenses' );
	
	if( !empty($options->wpmudev) ){
		$val = $options->wpmudev;
	}

	return $val;
}
add_filter( 'pre_option_wpmudev_apikey', __NAMESPACE__.'\pre_option_wpmudev_apikey', 10, 1 );

