<?php

namespace sjr;

/**
*
*/
class Timer{
	protected static $instance = NULL;

	protected $current_time = 0;
	protected $start_time = 0;

	/**
*
*/
	public static function instance(){
		if( !(self::$instance instanceof self) )
			self::$instance = new self;

		return self::$instance;
	}

	/**
	*
	*/
	protected function __construct(){
		$this->current_time = $this->start_time = microtime( TRUE );
	}

	/**
	*
	*	@return float
	*/
	public function seconds_elapsed(){
		$this->current_time = microtime( TRUE );

		return $this->current_time - $this->start_time;
	}
}