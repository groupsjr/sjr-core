<?php 

namespace sjr;

/**
*
*/
class Render{
	private $dir = '';
	private $template = '';

	/**
	*
	*	@param string absolute path to directory above /views/
	*/
	public function __construct( $dir ){
		$this->dir = $dir;
	}

	/**
	*	used to render a template from the plugins /views/ directory
	*	@param string template name in /views/ without .php
	*	@param object|array
	*	@return string html
	*/
	public function process( $template, $vars ){
		$this->template = $this->dir.'/views/'.$template.'.php';

		if( file_exists($this->template) ){
			ob_start();
			extract( (array) $vars, EXTR_SKIP );
			include $this->template;

			$html = ob_get_clean();
			return $html;
		}
	}
}