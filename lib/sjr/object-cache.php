<?php 

namespace sjr;

class Object_Cache extends \WP_Object_Cache{
	protected $cache = array(
		'site-transient' => array(),
		'transient' => array()
	);
	protected $table_name = '';						// wp_sjr_transients
	protected $time = 0;							// current timestamp
	protected $wpdb;								// shortcut to global

	public function __construct(){
		global $wpdb;

		$this->table_name = $wpdb->prefix.'sjr_transients';
		$this->time = time();
		$this->wpdb = &$wpdb;

		parent::__construct();
	}

	/**
	*
	*/
	public function get( $key, $group = 'default', $force = false, &$found = null ){ 
		if( in_array($group, array('site-transient', 'transient')) )
			return $this->get_transient( $key, $group );
		else
			return parent::get( $key, $group, $force, $found );
	}

	/**
	*
	*/
	public function set( $key, $data, $group = 'default', $expire = 0 ){
		if( in_array($group, array('site-transient', 'transient')) )
			return $this->set_transient( $key, $group, $data, $expire );
		else
			return parent::set( $key, $data, $group, $expire );
	}

	/**
	*
	*	@param string
	*	@param string
	*	@return mixed
	*/
	protected function get_transient( $key, $group ){
		if( isset($this->cache[ $group ][ $key ]) )
			return $this->cache[ $group ][ $key ];

		$sql = $this->wpdb->prepare( "SELECT * FROM $this->table_name
									  WHERE `key` = %s
									  AND `group` = %s
									  ORDER BY `expire` DESC
									  LIMIT 1", $key, $group, $this->time );

		$res = $this->wpdb->get_row( $sql );

		if( $res ){
			$data = @unserialize( $res->data );

			if( !$data && !in_array($res->data, array('a:0:{}', 'b:0;')) ){ 
				$this->remove_ids( array($res->id) );
				$data = NULL;
			}
		} else {
			$data = NULL;
		}

		return $data;
	}

	/**
	*
	*	@param string
	*	@param string
	*	@param mixed
	*	@param int
	*/
	protected function set_transient( $key, $group, $data, $expire = 0 ){
		if( $expire != 0 )
			$expire += $this->time;

		$data = serialize( $data );

		$sql = $this->wpdb->prepare( "INSERT INTO $this->table_name
									  ( `key`, `data`, `expire`, `group` )
									  VALUES
									  ( %s, %s, %d, %s )", $key, $data, $expire, $group );

		$this->wpdb->query( $sql );

		$this->cache[ $group ][ $key ] = $data;
	}

	/**
	*
	*	@param array
	*	@param array
	*	@return
	*/
	protected function remove_ids( $remove_ids = array(), $keep_ids = array() ){
		$remove = array_diff( $remove_ids, $keep_ids );

		if( $remove ){
			$remove = implode( ', ', $remove );

			$sql = "DELETE FROM $this->table_name
					WHERE `id` IN( $remove )";

			$this->wpdb->query( $sql );
		}
	}
}