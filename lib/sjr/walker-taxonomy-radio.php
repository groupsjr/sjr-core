<?php

namespace sjr;

class Walker_Taxonomy_Radio extends \Walker{
	public $db_fields = array(
		'parent' => 'parent', 
		'id' => 'term_id'
	);
	public $tree_type = 'category';

	/**
	*
	*/
	public function __construct(){
		add_filter( 'get_terms', array($this, 'get_terms'), 10, 4 );
	}

	/**
	*	adds '[ None ]' to terms checklist in admin to allow deselection
	*	@param array
	*	@param array
	*	@param array
	*	@param \WP_Term_Query
	*	@return array
	*/
	public function get_terms( $terms, $taxonomy, $query_vars, $term_query ){
		$options = get_sjr_option( 'taxonomies' );

		if( array_intersect($taxonomy, array_flip(array_filter($options->single))) ){
			array_unshift( $terms, (object) array(
				'term_id' => -1,
				'name' => '[ None ]'
			) );
		}

		return $terms;
	}

	/**
	*
	*	@param strng
	*	@param
	*	@param int
	*	@param
	*	@param
	*/
	public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ){
		extract( $args );
		
        if( empty($taxonomy) )
            $taxonomy = 'category';

        if( $taxonomy == 'category' )
            $name = 'post_category';
        else
            $name = 'tax_input['.$taxonomy.']';

        $class = in_array( $category->term_id, $popular_cats ) ? ' class="popular-category"' : '';

        $output .= sprintf( '<li id="%s-%s" %s>', $taxonomy, $category->term_id, $class );
        $output .= sprintf( '<label class="selectit">
        						<input value="%d" type="radio" name="%s[]" id="in-%s-%d" %s %s />
        						%s
        					</label>', 
        					$category->term_id, 
        					$name, 
        					$taxonomy, 
        					$category->term_id, 
        					checked( in_array( $category->term_id, $selected_cats ), TRUE, FALSE ), 
        					disabled( empty( $args['disabled'] ), FALSE, FALSE ), 
        					esc_html( apply_filters('the_category', $category->name) ) );
	}

	/**
	*
	*	@param string
	*	@param int
	*	@param
	*/
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		$indent = str_repeat("\t", $depth);
        $output .= $indent.'<ul class="children">';
	}

	/**
	*
	*	@param strng
	*	@param
	*	@param int
	*	@param
	*	@param
	*/
	public function end_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ){
		$output .= '</li>';
	}

	/**
	*
	*	@param string
	*	@param int
	*	@param
	*/
	public function end_lvl( &$output, $depth = 0, $args = array() ){
        $indent = str_repeat("\t", $depth);
        $output .= $indent.'</ul">';
    }
}