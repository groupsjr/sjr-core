<?php

namespace sjr;

class W3_Plugin_CDN extends \W3_Plugin_Cdn{
    /**
     *  OB Callback
     *  Copied from \W3_Plugin_Cdn to access private make_uploads_regexes method
     *
     *  @param string $buffer
     *  @return string
     */
    function ob_callback(&$buffer) {
        if ($buffer != '' && w3_is_xml($buffer) ){
            if ($this->can_cdn2($buffer) ){
                $regexps = array();
                $site_path = w3_get_site_path();
                $domain_url_regexp = w3_get_domain_url_regexp();
                $site_domain_url_regexp = false;
                if ($domain_url_regexp != w3_get_url_regexp(w3_get_domain(w3_get_site_url())))
                    $site_domain_url_regexp = w3_get_url_regexp(w3_get_domain(w3_get_site_url()));

                if ($this->_config->get_boolean('cdn.uploads.enable') ){
                    w3_require_once(W3TC_INC_DIR . '/functions/http.php');

                    $upload_info = w3_upload_info();

                    if ($upload_info) {
                        $baseurl = $upload_info['baseurl'];

                        if (defined('DOMAIN_MAPPING') && DOMAIN_MAPPING) {
                            $parsed = @parse_url($upload_info['baseurl']);
                            $baseurl = home_url() . $parsed['path'];
                        }

                        $regexps = $this->make_uploads_regexes($domain_url_regexp, $baseurl, $upload_info, $regexps);
                        if ($site_domain_url_regexp)
                            $regexps = $this->make_uploads_regexes($site_domain_url_regexp, $baseurl, $upload_info, $regexps);
                    }
                }

                if ($this->_config->get_boolean('cdn.includes.enable') ){
                    $mask = $this->_config->get_string('cdn.includes.files');
                    if ($mask != '') {
                        $regexps[] = '~(["\'(])\s*((' . $domain_url_regexp . ')?(' . w3_preg_quote($site_path . WPINC) . '/(' . $this->get_regexp_by_mask($mask) . ')))~';
                        if ($site_domain_url_regexp)
                            $regexps[] = '~(["\'(])\s*((' . $site_domain_url_regexp . ')?(' . w3_preg_quote($site_path . WPINC) . '/(' . $this->get_regexp_by_mask($mask) . ')))~';
                    }
                }

                if ($this->_config->get_boolean('cdn.theme.enable') ){
                    $theme_dir = preg_replace('~' . $domain_url_regexp . '~i', '', get_theme_root_uri());

                    $mask = $this->_config->get_string('cdn.theme.files');

                    if ($mask != '') {
                        $regexps[] = '~(["\'(])\s*((' . $domain_url_regexp . ')?(' . w3_preg_quote($theme_dir) . '/(' . $this->get_regexp_by_mask($mask) . ')))~';
                        if ($site_domain_url_regexp) {
                            $theme_dir2 = preg_replace('~' . $site_domain_url_regexp. '~i', '', get_theme_root_uri());
                            $regexps[] = '~(["\'(])\s*((' . $site_domain_url_regexp . ')?(' . w3_preg_quote($theme_dir) . '/(' . $this->get_regexp_by_mask($mask) . ')))~';
                            $regexps[] = '~(["\'(])\s*((' . $site_domain_url_regexp . ')?(' . w3_preg_quote($theme_dir2) . '/(' . $this->get_regexp_by_mask($mask) . ')))~';
                        }
                    }
                }

                if ($this->_config->get_boolean('cdn.custom.enable') ){
                    $masks = $this->_config->get_array('cdn.custom.files');
                    $masks = array_map(array($this, '_replace_folder_placeholders'), $masks);
                    $masks = array_map('w3_parse_path', $masks);

                    if (count($masks) ){
                        $mask_regexps = array();

                        foreach ($masks as $mask) {
                            if ($mask != '') {
                                $mask = w3_normalize_file($mask);
                                $mask_regexps[] = $this->get_regexp_by_mask($mask);
                            }
                        }

                        $regexps[] = '~(["\'(])\s*((' . $domain_url_regexp . ')?(' . w3_preg_quote($site_path) . '(' . implode('|', $mask_regexps) . ')))~i';
                        if ($site_domain_url_regexp)
                            $regexps[] = '~(["\'(])\s*((' . $site_domain_url_regexp . ')?(' . w3_preg_quote($site_path) . '(' . implode('|', $mask_regexps) . ')))~i';
                    }
                }

                foreach ($regexps as $regexp) {
                    $buffer = preg_replace_callback($regexp, array(
                        &$this,
                        'link_replace_callback'
                    ), $buffer);
                }

                if ($this->_config->get_boolean('cdn.minify.enable') ){
                    if ($this->_config->get_boolean('minify.auto') ){
                        $regexp = '~(["\'(])\s*' .
                            $this->_minify_url_regexp('/[a-zA-Z0-9-_]+\.(css|js)') .
                            '~U';
                        if (w3_is_cdn_mirror($this->_config->get_string('cdn.engine')))
                            $processor = 'link_replace_callback';
                        else
                            $processor = 'minify_auto_pushcdn_link_replace_callback';
                    } else {
                        $regexp = '~(["\'(])\s*' .
                            $this->_minify_url_regexp('/[a-z0-9]+/.+\.include(-(footer|body))?(-nb)?\.[a-f0-9]+\.(css|js)') .
                            '~U';
                        $processor = 'link_replace_callback';
                    }

                    $buffer = preg_replace_callback($regexp, array(
                        &$this,
                        $processor),
                        $buffer);
                }
            }

            if ($this->_config->get_boolean('cdn.debug') ){
                $buffer .= "\r\n\r\n" . $this->get_debug_info();
            }
        }

        return $buffer;
    }

	/**
     *  Modified to match on white space before url for responsive images
     *  @param $domain_url_regexp
     *  @param $baseurl
     *  @param $upload_info
     *  @param $regexps
     *  @return array
     */
    private function make_uploads_regexes( $domain_url_regexp, $baseurl, $upload_info, $regexps ){
        if( preg_match('~' . $domain_url_regexp . '~i', $baseurl) ){
            $regexps[] = '~(["\'(\s])\s*((' . $domain_url_regexp . ')?(' . w3_preg_quote($upload_info['baseurlpath']) . '([^"\'\s)>]+)))~';
        } else {
            $parsed = @parse_url($baseurl);
            $upload_url_domain_regexp = isset($parsed['host']) ? w3_get_url_regexp($parsed['scheme'] . '://' . $parsed['host']) : $domain_url_regexp;
            $baseurlpath = isset($parsed['path']) ? rtrim($parsed['path'], '/') : '';
            if ($baseurlpath)
                $regexps[] = '~(["\'])\s*((' . $upload_url_domain_regexp . ')?(' . w3_preg_quote($baseurlpath) . '([^"\'>]+)))~';
            else
                $regexps[] = '~(["\'])\s*((' . $upload_url_domain_regexp . ')(([^"\'>]+)))~';
        }
        
        return $regexps;
    }
}

