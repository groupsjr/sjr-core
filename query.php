<?php

namespace sjr;

/**
*	filter for inline sticky to not query for sticky posts after main query has run
*	@param WP_Query
*	@return WP_Query
*/
function pre_get_posts_inline_sticky( $wp_query ){
	if( $wp_query->is_main_query() ){
		$wp_query->set( 'ignore_sticky_posts', TRUE );
	}

	return $wp_query;
}

/**
*	set the posts per page if enabled in admin.php?page=sjr-core_site
*	@param WP_Query
*	@param WP_Query
*/
function pre_get_posts_posts_per_page( $wp_query ){
	// empty( $wp_query->get('posts_per_page') ) does not work on php < 5.5
	$is_main_query = $wp_query->is_main_query();
	$posts_per_page = $wp_query->get( 'posts_per_page' );

	if( $is_main_query && empty($posts_per_page) ){
		$site_options = get_sjr_option( 'site' );

		foreach( $site_options->posts_per_page as $condition => $per_page ){
			if( call_user_func(array($wp_query, $condition)) ){
				$wp_query->set( 'posts_per_page', $per_page );
				break;
			}
		}
	}

	return $wp_query;
}

/**
*	filter for inline sticky to put stickied at top
*	can use 'inline_sticky' => 1 as query arg
*	@param string
*	@param WP_Query
*	@return string
*/
function posts_orderby_inline_sticky( $sql, $wp_query ){
	global $wpdb;

	if( $wp_query->is_main_query() || $wp_query->get('inline_sticky') ){
		$sticky_posts = (array) get_option( 'sticky_posts' );

		if( !empty($sticky_posts) ){
			$sticky_posts = implode( ', ', array_map('intval', $sticky_posts) );
			$sql = "$wpdb->posts.ID IN( $sticky_posts ) DESC, ".$sql;
		}	
	}

	return $sql;
}

/**
*	support post_name__in in orderby 
*	@param string
*	@param WP_Query
*	@return string
*/
function posts_orderby_post_name__in( $sql, $wp_query ){
	global $wpdb;

	$orderby = $wp_query->get( 'orderby' );
	
	if( is_string($orderby) && strcasecmp($orderby, 'post_name__in') === 0 ){
		$post_name__in = $wp_query->get( 'post_name__in' );

		// this is sanitized already!
		$post_name__in = implode( ', ', array_map(function($post_name) use($wpdb){
			return $wpdb->prepare( '%s', $post_name );
		}, $post_name__in) );

		$sql = "FIELD( $wpdb->posts.post_name, $post_name__in )";
	}

	return $sql;
}
add_filter( 'posts_orderby', __NAMESPACE__.'\posts_orderby_post_name__in', 10, 2 );

/**
*	force RAND( d ) in orderby when RAND() is used
*	also allowed 'orderby' => array( 'rand', x ) before implemented in WP 4.5
*	@param string
*	@param WP_Query
*	@return string
*/
function posts_orderby_rand( $sql, $wp_query ){
	global $wpdb;

	$orderby = $wp_query->get('orderby');
	
	if( is_string($orderby) && strcasecmp($orderby, 'rand') === 0 ){
		$sql = $wpdb->prepare( 'RAND( %d )', rand(0, 10) );
	} elseif( is_array($orderby) && strcasecmp($orderby[0], 'rand') === 0 ){
		_deprecated_argument( __NAMESPACE__.'\posts_orderby_rand', '4.5', 'RAND(d)' );

		$sql = $wpdb->prepare( 'RAND( %d )', $orderby[1] );
	}

	return $sql;
}
add_filter( 'posts_orderby', __NAMESPACE__.'\posts_orderby_rand', 10, 2 );

/**
*	set the 404 flag correctly when certain permalink structures are being used
*	example /%category%/%postname%/
*	/dsdada/?s=weeweqw
*	@param array
*	@param WP_Query
*	@return array
*/
function the_posts_set_404( $posts, \WP_Query $wp_query ){
	if( $wp_query->is_main_query() && !count($posts) && ($wp_query->is_singular() || $wp_query->is_category() || $wp_query->is_tag() || $wp_query->is_tax() ) ){
		$wp_query->set_404();
		status_header( 404 );
	}

	return $posts;
}
add_filter( 'the_posts', __NAMESPACE__.'\the_posts_set_404', 10, 2 );

/**
*	for debugging posts query
*	@param string
*	@param WP_Query
*	@return string
*/
function _posts_request_debug( $sql, \WP_Query $wp_query ){
	//if( $wp_query->is_main_query() ){
		//dbug( $sql, '$sql' );
	//}

	return $sql;
}
add_filter( 'posts_request', __NAMESPACE__.'\_posts_request_debug', PHP_INT_MAX, 2 );