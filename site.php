<?php

namespace sjr;

/**
*	replace W3_Plugin_CDN global with sjr extended for responsive srcset in cdn 
*	attached to `init` action
*/
function check_w3_cdn(){
	if( !empty($GLOBALS['_w3tc_ob_callbacks']['cdn']) && $GLOBALS['_w3tc_ob_callbacks']['cdn'][0] instanceof \W3_Plugin_CDN )
		$GLOBALS['_w3tc_ob_callbacks']['cdn'][0] = new W3_Plugin_CDN;
}

/**
*	show 'None' in default category on wp-admin/options-writing.php
*	attached to `get_terms` filter
*/
function get_terms_allow_none( $terms, $taxonomies, $args ){
	if( empty($args['show_option_none']) && !empty($args['class']) && $args['class'] == 'postform' ){
		array_unshift( $terms, (object) array(
			'name' => '[ None ]',
			'parent' => NULL,
			'slug' => '-1',
			'taxonomy' => reset( $taxonomies ),
			'term_id' => -1
		) );
	}

	return $terms;
}
add_filter( 'get_terms', __NAMESPACE__.'\get_terms_allow_none', 10, 3 );

/**
*	implement 401 authentication 
*	attached to `plugins_loaded` action
*	@see https://secure.php.net/manual/en/features.http-auth.php
*	@return bool
*/
function http_auth(){
	$realm = '- Restricted area -';

	$options = get_sjr_option( 'site' );

	if( empty($_SERVER['PHP_AUTH_DIGEST']) ){
		http_auth_send_401( $options->password['realm'] );
	} else {
		$data = http_auth_digest_parse( $_SERVER['PHP_AUTH_DIGEST'] );

		$users = array();

		// format users to user => pass
		array_map( function($creds) use(&$users){
			$users[$creds['user']] = $creds['pass'];
		}, $options->password['users'] );

		$user = isset( $users[$data['username']] ) ? $users[$data['username']] : '';

		$a1 = md5( $data['username'] . ':' . $realm . ':' . $user );
		$a2 = md5( $_SERVER['REQUEST_METHOD'].':'.$data['uri'] );
		$valid_response = md5( $a1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$a2 );

		if( $data['response'] != $valid_response )
    		http_auth_send_401( $realm );
	}

	return TRUE;
}

/**
*	logs email sent via standard wp_mail function to be logged if dbug is active
*	@param
*	@return
*/
function log_email( &$phpmailer ){
	if( function_exists('dlog') ){
		$to = $phpmailer->getToAddresses();
		$to = array_map( function($addr){
			return sprintf( '%s <%s>', $addr[1], $addr[0] );
		},  $to );

		$vars = array(
			'from' => $phpmailer->From,
			'to' => implode( ', ', $to ),
			'body' => $phpmailer->Body
		);

		$email = render( 'log_email', $vars );
		dlog( $email, 'email', 'email_log' );
	}
}

/**
*	show direct link to media asset
*	@param array
*	@param WP_Post
*	@param bool
*	@return array
*/
function media_row_actions( $actions, $post, $detached ){
	$actions['direct'] = sprintf( '<a href="%s"><nobr>Direct Link</nobr></a>', \wp_get_attachment_url($post->ID) );

	return $actions;
}

/**
*	force robots to disallow site on dev urls
*	attached to `option_blog_public` filter
*	@param string site default value
*	@return string filtered value
*/
function option_blog_public( $val ){
	$site_options = get_sjr_option( 'site' );

	foreach( $site_options->robots_hosts as $host ){
		if( stripos($_SERVER['HTTP_HOST'], $host) !== FALSE ){
			return '0';
		}
	}
	
	return $val;
}

/**
*	fixes an error when `default_category` option is set to a term that does not exist, and get_term returns null rather than \WP_Error
*	@param string
*	@param string
*	@return int
*/
function option_default_category( $term_id = '1', $option_name = 'default_category' ){
	$term_id = intval( $term_id );
	$term = get_term( $term_id, 'category' );

	if( !$term )
		$term_id = 0;

	return $term_id;
}

/**
*	 attached to `init` action to remove meta tags from `wp_head` action
*/
function remove_head_elements(){
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_generator' );
}

/**
*	searches post content for broken media library images and replace
*	used in `Fix Broken Images` Tool
*	@param object with ID and post_content properties
*	@return
*/
function replace_broken_images( $post ){
	$content = $post->post_content;
	$replaced = 0;

	$imgs = get_tags_from_html( $content, 'img' );
	
	foreach( $imgs as $img ){
		$class = $img->getAttribute( 'class' );

		preg_match( '/wp-image-([0-9]+)/', $class, $matches );
		$image_id = isset( $matches[1] ) ? intval( $matches[1] ) : 0;

		if( !$image_id )
			continue;

		$src = $img->getAttribute( 'src' );
		$filename = pathinfo( $src, PATHINFO_BASENAME );

		$meta = wp_get_attachment_metadata( $image_id );
		if( !$meta || !isset($meta['sizes']) )
			continue;

		$files = wp_list_pluck( $meta['sizes'], 'file' );

		if( in_array($filename, $files) )
			continue;

		$height = intval( $img->getAttribute('height') );
		$width = intval( $img->getAttribute('width') );

		// get the square pixels for each image, and use the next biggest one if it exists,
		$ordered_sizes = array_map( function($size){
			return $size['width'] * $size['height'];
		}, $meta['sizes'] );

		asort( $ordered_sizes );

		// or the next smallest one if not
		$area = $height * $width;
		$bigger_sizes = array_filter( $ordered_sizes, function($size) use ($area){
			return $size >= $area;
		} );

		if( $bigger_sizes ){
			// if there are bigger sizes, then use the next size up
			$bigger_sizes = array_keys( $bigger_sizes );
			$best = reset( $bigger_sizes );
		} else {
			// use the biggest image there is 
			$all_sizes = array_keys( $ordered_sizes );
			$best = end( $all_sizes );
		}

		$new_file = $meta['sizes'][$best]['file'];
		
		// save replacement data
		add_post_meta( $post->ID, '_sjr_image_scan_file', array(
			'original' => $filename,
			'new' => $new_file
		) );

		$content = str_replace( $filename, $new_file, $content );
		$replaced++;
	}
	
	if( $replaced > 0 ){
		wp_update_post( array(
			'ID' => $post->ID,
			'post_content' => $content
		) );
	}

	update_post_meta( $post->ID, '_sjr_image_scan_replaced', $replaced );
	update_post_meta( $post->ID, '_sjr_image_scan_last', current_time('timestamp') );
}

/**
*	attached to `admin_url` filter
*	replace /wp-admin/admin-ajax.php with /ajax/ 
*	@param string
*	@param string
*	@param int
*	@return string
*/
function rewrite_ajax_admin_url( $url, $path, $blog_id ){
	if( $path == 'admin-ajax.php' )
		$url = get_site_url( $blog_id, '/ajax/' );

	return $url;
}

/**
*	attached to `pre_get_posts` for request to /ajax/ to be intercepted and include admin-ajax.php
*	@param WP_Query
*	@return WP_Query
*/
function rewrite_ajax_pre_get_posts( $wp_query ){
	if( $wp_query->is_main_query() && $wp_query->get('sjr_ajax') ){
		require ABSPATH.'/wp-admin/admin-ajax.php';
		exit();
	}

	return $wp_query;
}

/**
*	attached to `query_vars` filter to register `sjr_ajax` 
*	@param array
*	@return array
*/
function rewrite_ajax_query_vars( $qv ){
	array_push( $qv, 'sjr_ajax' );

	return $qv;
}

/**
*	attatched to `root_rewrite_rules` to register rewrite rule for /ajax/
*	@param array
*	@return array
*/
function rewrite_ajax_root_rewrite_rules( $rules ){
	$rules['ajax/?$'] = 'index.php?&sjr_ajax=1';

	return $rules;
}

/**
*	starts output buffering to show serialized options in /wp-admin/options.php
*	attached to `admin_head-options.php` action
*/
function serialized_options_head(){ 
	add_action( 'wp_after_admin_bar_render', function(){
		ob_start();
	} );
	add_action( 'admin_print_footer_scripts', __NAMESPACE__.'\serialized_options_foot' );
}

/**
*	replace disabled options /wp-admin/options.php with raw serialized data
*	attached to `admin_footer-options.php` action
*/
function serialized_options_foot(){
	$html = ob_get_clean();
	$doc = get_html_dom( $html );
	$tags = get_tags_from_html( $doc, 'input' );

	$i = $tags->length - 1; 

	// https://php.net/manual/en/domnode.replacechild.php#50500
	while( $i > -1 ){ 
		$tag = $tags->item( $i );
		if( $tag->getAttribute('disabled') == 'disabled' ){
			$option_name = $tag->getAttribute( 'name' );
			$option_value = get_option( $option_name );

			$pre = $doc->createElement( 'pre', esc_html(print_r($option_value, TRUE)) ); 

			$tag->parentNode->replaceChild( $pre, $tag );
		}

		$i--;
	}

	echo $doc->saveHTML();
}

/**
*	filter title to allowed html tags
*	attached to `the_title` filter
*	@param string
*	@param int
*	@return string
*/
function the_title_title_tags( $title, $post_id = 0 ){
	static $allowed_tags, $config;

	$options = get_sjr_option( 'site' );

	if( is_null($config) ){
		$config = get_yaml( $options->title_tags['config'] );

		// config keys need to be arrays for wp_kses
		array_walk_recursive( $config, function(&$value, &$key){
			$value = array();
		}, $config );
	}

	if( is_null($allowed_tags) ){
		$allowed_tags = implode( '', array_map(function($tag){
			return sprintf( '<%s>', $tag );
		}, array_keys($config)) );
	}

	if( $options->title_tags['remove'] ){
		$title = strip_tags_content( $title, $allowed_tags );
	}

	$title = wp_kses( $title, $config );


	return $title;
}

/**
*	attached to `wp_insert_post_data` filter for `Validate HTML on post save` setting
*	@param array $data    An array of slashed post data.
*	@param array $postarr An array of sanitized, but otherwise unmodified post data.
*	@return array
*/
function wp_insert_post_data_validate_html( $data, $postarr ){
	$post_content = stripslashes( $data['post_content'] );
	$post_content = get_html_dom( $post_content );
	
	$post_content = get_html_body( $post_content );

	$data['post_content'] = addslashes( $post_content );

	return $data;
}

