<?php

namespace sjr;

/**
*
*	@param mixed
*	@param int
*	@param array
*	@return array
*/
function acf_images_copy( $value, $post_id, $field ){
	if( is_numeric($value) ){

	} elseif( is_string($value) ){
		$local_file = attachment_url_to_path( $value );

		if( !file_exists($local_file) )
			copy_file_from_url( $value, $local_file );
	} elseif( is_array($value) ){
		$sizes = get_intermediate_image_sizes();

		foreach( $sizes as $size ){
			$url = $value['sizes'][$size];
			$local_file = attachment_url_to_path( $url );

			if( !file_exists($local_file) )
				copy_file_from_url( $url, $local_file );
		}
	} elseif( WP_DEBUG ){

	}

	return $value;
}

/**
*
*	@param mixed
*	@param int
*	@param array
*	@return array
*/
function acf_images_placeholder( $value, $post_id, $field ){
	$images_options = get_sjr_option( 'images' );
	$sizes = get_intermediate_image_sizes();

	if( is_string($value) ){
		
		$local_file = attachment_url_to_path( $value );
		if( !$local_file )
			return $value;

		$size = $field['preview_size'];

		if( !file_exists($local_file) ){
			$value = get_placeholder_image();
		}

	} elseif( is_array($value) ){
	
		foreach( $sizes as $size ){
			$url = $value['sizes'][$size];
			$local_file = attachment_url_to_path( $url );

			if( $images_options->placeholder_force || !file_exists($local_file) ){
				$dims = get_image_dims( $size );
				$placeholder = get_placeholder_image( $dims['width'], $dims['height'] );

				$value['sizes'][$size] = $placeholder;
			}
		}
	} elseif( WP_DEBUG ){

	}

	return $value;
}

/**
*	copy uploaded images from a chunk of html to the local server
*	attached to `post_thumbnail_html` and `the_content` filters
*	@param string
*	@return string
*/
function html_scan_images_copy( $html ){
	$transient_key = transient_key( 'sjr-core-html-scan', array($html) );
	$to_copy = get_transient( $transient_key );

	if( $to_copy !== '0' ){
		$to_copy = 0;
		$tags = get_tags_from_html( $html, 'img' );

		foreach( $tags as $tag ){
			$url = $tag->getAttribute( 'src' );
			$local_file = attachment_url_to_path( $url );

			if( trim($local_file) && !file_exists($local_file) ){
				$to_copy ++;

				copy_file_from_url( $url, $local_file );
			}
		}

		set_transient( $transient_key, $to_copy, HOUR_IN_SECONDS );
	}

	return $html;
}

/**
*	replace images in chunk of html with placeholder images
*	attached to `acf/format_value/type=wysiwyg`, `the_content` filters
*	@param string
*	@return string
*/
function html_scan_images_placeholder( $html ){
	$images_options = get_sjr_option( 'images' );
	$tags = get_tags_from_html( $html, 'img' );

	foreach( $tags as $tag ){
		$url = $tag->getAttribute( 'src' );
		$local_file = attachment_url_to_path( $url );
		
		if( $images_options->placeholder_force || !file_exists($local_file) ){
			// if we dont know image size of inline content, just have to guess
			$height = (int) $tag->getAttribute( 'height' );
			$width = (int) $tag->getAttribute( 'width' );

			if( !$height || !$width )
				$height = $width = 2048;

			$placeholder = get_placeholder_image( $width, $height );
			
			$html = str_replace( $url, $placeholder, $html );
		}
	}

	return $html;
}

/**
*	add ?c_mdt param to break cache
*	@param array
*	@param int
*	@return array
*/
function image_cachebreaker_metadata( $metadata, $post_id ){
	$cache_data = substr( md5(serialize($metadata)), 0, 8 );

	foreach( $metadata['sizes'] as &$size ){
		$size['file'] = add_query_arg( array(
			'c_mdt' => $cache_data
		), $size['file'] );
	}

	return $metadata;
}

/**
*	add ?r_mdt param to break cache
*	@param array
*	@param int
*	@return array
*/
function image_cachebreaker_metadata_random( $metadata, $post_id ){
	static $cache_data;
	if( !$cache_data ) 
		$cache_data = substr( md5(microtime()), 0, 8 );

	foreach( $metadata['sizes'] as &$size ){
		$size['file'] = add_query_arg( array(
			'r_mdt' => $cache_data
		), $size['file'] );
	}

	return $metadata;
}

/**
*	add ?c_url param to break cache
*	@param string
*	@param int
*	@return string
*/
function image_cachebreaker_url( $url, $post_id ){
	$metadata = wp_get_attachment_metadata( $post_id );

	$url = add_query_arg( array(
		'c_url' => substr( md5(serialize($metadata)), 0, 8 )
	), $url );

	return $url;
}

/**
*	add ?r_url param to break cache
*	@param string
*	@param int
*	@return string
*/
function image_cachebreaker_url_random( $url, $post_id ){
	static $cache_data;
	if( !$cache_data ) 
		$cache_data = substr( md5(microtime()), 0, 8 );

	$url = add_query_arg( array(
		'r_url' => $cache_data
	), $url );

	return $url;
}

/**
*	copy images in post content from external hosts eg. imgur
*	@TODO improve performance using cache with wp_transient
*	@param string
*	@return string
*/
function images_copy_offsite( $html ){
	global $post;

	$changes_made = 0;
	$images_options = get_sjr_option( 'images' );

	$transient_key = transient_key( 'sjr-core-copy-offsite', array($html, $images_options->offsite_include) );
	$imgs_found = get_transient( $transient_key );

	if( $imgs_found !== '0' ){
		require_once ABSPATH . 'wp-admin/includes/media.php';
		require_once ABSPATH . 'wp-admin/includes/file.php';
		require_once ABSPATH . 'wp-admin/includes/image.php';

		$imgs_found = 0;
		$imgs = get_tags_from_html( $html, 'img' );

		foreach( $imgs as $img ){
			$src = $img->getAttribute( 'src' );
			$alt = $img->getAttribute( 'alt' );
			$host = parse_url( $src, PHP_URL_HOST );

			// only copy if specific servers arent set, or host is in include list
			// @TODO make sure images from this host  dev / staging envs are not copied
			if( $host && in_array($host, $images_options->offsite_include) ){
				$imgs_found++;

				$img = media_sideload_image( $src, $post->ID, $alt, 'src' );
				if( !is_wp_error($img) ){
					$changes_made ++;
					$html = str_replace( $src, $img, $html );
				}
			}
		}

		if( $changes_made > 0 ){
			$update = array(
				'ID' => $post->ID,
				'post_content' => $html
			);

			wp_update_post( $update );
		}

		set_transient( $transient_key, $imgs_found, HOUR_IN_SECONDS );
	}
	
	return $html;
}

/**
*	copys image from other environment, used when requesting a cropped/sized version
*	attached to `image_downsize` filter
*	@param bool
*	@param int
*	@param string
*	@return
*/
function image_downsize_copy( $false, $id, $size ){
	$intermediate = image_get_intermediate_size( $id, $size );

	if( !$intermediate || !isset($intermediate['url']) )
		return $false;

	$local_file = attachment_url_to_path( $intermediate['url'] );

	if( $local_file && !file_exists($local_file) )
		copy_file_from_url( $intermediate['url'], $local_file );

	return $false;
}

/*
*	returns a placeholder image, used when requesting a cropped/sized version
*	attached to `image_downsize` filter
*	@param bool
*	@param int
*	@param string
*	@return bool | array
*/
function image_downsize_placeholder( $return, $id, $size ){
	$dims = get_image_dims( $size );

	if( $dims ){
		$return = array(
			get_placeholder_image( $dims['width'], $dims['height'] ),
			$dims['width'], 
			$dims['height']
		);
	}

	return $return;
}

/*
*	for mocking thumbnail images
*	note - this can not work using the wp_get_attachment_url filter because of limitations with image properties available
*	@param string html image tag
*	@param int parent post id
*	@param int attachment id
*	@param string 
*	@param string
*	@return string
*/
function post_thumbnail_html_placeholder( $html, $post_id = 0, $post_thumbnail_id = 0, $size = '', $attr = '' ){
	$item = get_tags_from_html( $html, 'img' )->item( 0 );
	if( !$item )
		return $html;

	$url = $item->getAttribute( 'src' );

	$local_file = attachment_url_to_path( $url );
	$images_options = get_sjr_option( 'images' );

	if( $images_options->placeholder_force || !file_exists($local_file) ){
		$dims = get_image_dims( $size );
		$placeholder = get_placeholder_image( $dims['width'], $dims['height'] );

		$html = str_replace( $url, $placeholder, $html );
	}

	return $html;
}

/**
*	fix for force regenerate thumbnails when image is on a diferent cdn than site is configured for
*	
*/
function regeneratethumbnail_s3_fix(){
	global $as3cf;

	if( !($as3cf instanceof \Amazon_S3_And_CloudFront) )
		return;

	$id = intval( $_REQUEST['id'] );
	$url = get_attached_file( $id );

	$wp_upload_dir = wp_upload_dir();
	$baseurl = $wp_upload_dir['baseurl'];

	if( strpos($url, $baseurl) !== 0 ){
		$info = $as3cf->get_attachment_s3_info( $id );

		if( isset($info['key']) ){
			$key = $info['key'];
			$pathinfo = pathinfo( $key );

			// create the directory, /uploads/yyyy/mm etc
			$path = ABSPATH.$pathinfo['dirname'];
			wp_mkdir_p( $path );
			
			// download to temp file and move to created directory
			$tmp_file = download_url( $url );
			$new_file = $path.'/'.$pathinfo['basename'];
			rename( $tmp_file, $new_file );
		}
	}
}

/**
*	runs on 404 page to copy image from other environment, send to amazon s3, and redirect
*	@param array
*	@param WP_Query
*	@return array
*/
function s3_copy_missing_image( $posts, $wp_query ){
	global $as3cf, $wpdb;
	
	if( $wp_query->is_404() ){

		// the path ot the file requested, ex /wp-content/uploads/2015/08/test.jpg
		$request_uri = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );

		$transient_key = transient_key( 'sjr-core-s3_copy', array($request_uri) );
		$s3_url = get_transient( $transient_key );
		
		if( $s3_url === FALSE ){
			if( !($as3cf instanceof \Amazon_S3_And_CloudFront) )
				return $posts;

			// amazon settings
			$bucket = $as3cf->get_setting( 'bucket' );
			$region = $as3cf->get_setting( 'region' );
			$acl = $as3cf::DEFAULT_ACL;

			$s3client = $as3cf->get_s3client( $region, FALSE );
			if( !($s3client instanceof \Aws\S3\S3Client) )
				return $posts;

			// file contents of image
			$image = get_file_from_url( $request_uri );
			if( !($image) )
				return $posts;

			$args = array(
				'Bucket' => $bucket,
				// @TODO
				// 'ContentType'  => mime_content_type( $file ),
				'Key' => $request_uri,
				'Body' => $image->file_contents, 
				'ACL' => $acl,
			);

			$ok = $s3client->putObject( $args );

			$s3_url = $ok->get( 'ObjectURL' );
			
			$sql = $wpdb->prepare( "UPDATE $wpdb->posts P 
									SET P.post_content = REPLACE( P.post_content, %s, %s )
									WHERE P.post_status = 'publish'", site_url($request_uri), $s3_url );
			$wpdb->query( $sql );

			set_transient( $transient_key, $s3_url, MINUTE_IN_SECONDS );
		}
		
		if( $s3_url ){
			wp_redirect( $s3_url, 301 );
			die();
		}
	}

	return $posts;
}

/**
*	check if each img in the srcset array exists and copy if not
*	@param array
*	@param array
*	@param string absolute url of full size image
*	@param array
*	@param int
*	@return array
*/
function srcset_images_copy($sources, $size_array, $image_src, $image_meta, $attachment_id){
	$imgs = wp_list_pluck( $sources, 'url' );
	array_map( '\sjr\url_image_copy' , $imgs );

	return $sources;
}

/**
*
*	@param string
*	@param int
*	@return string
*/
function url_image_copy( $url, $post_id = 0 ){
	$local_file = attachment_url_to_path( $url );

	if( $local_file && !file_exists($local_file) )
		copy_file_from_url( $url, $local_file );
	
	return $url;
}

/*
*
*	@param string
*	@param int
*	@return string
*/
function url_image_placeholder( $url, $post_id = 0 ){
	$data = wp_get_attachment_metadata( $post_id );

	if( is_array($data) )
		$url = get_placeholder_image( $data['width'], $data['height'] );

	return $url;
}

/**
*	add sizes array to image metadata if does not exist.  
*	compat for cloudinary and smush pro active together
*	@param array
*	@param int
*	@return array
*/
function wp_get_attachment_metadata_sizes_default( $data, $post_id ){
	if( is_array($data) && !isset($data['sizes']) )
		$data['sizes'] = array();

	return $data;
}

/**
*	make image urls protocol relative 
*	@param string
*	@param int
*	@return string
*/
function wp_get_attachment_url( $url, $post_id ){
	if( is_ssl() && stripos($url, 'http://') === 0 )
		$url = substr( $url, 5 );

	return $url;
}

/**
*	compat for Animated GIF Resize and Force Regenerate Thumbnails
*	@param array
*	@return array
*/
function wp_image_editors_animated_gif_fix( $editors ){
	$index = array_search( 'Bbpp_Animated_Gif', $editors );

	if( $index !== FALSE ){
		unset( $editors[$index] );
		array_unshift( $editors, 'Bbpp_Animated_Gif' );
	}

	return $editors;
}
