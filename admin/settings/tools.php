<?php

namespace sjr;

/**
*   render button to clear transients
*/
function field_tools_delete_term(){
    echo render( 'admin/settings/tools-term' );
}

/**
*   Shows data from image replacements performed from `Fix Broken Images` tool
*/
function field_tools_fix_images(){
    $posts = get_image_replaced_posts();
    foreach( $posts as &$post ){
        $post->replacements = $post->last_updated ? get_post_meta( $post->ID, '_sjr_image_scan_file' ) : array();
    }   

    echo render( 'admin/settings/tools-fix-images', array(
        'posts' => $posts
    ) );
}

/**
*   render button to clear transients
*/
function field_tools_transients(){
    echo render( 'admin/settings/tools-transients' );
}

/**
*   render description for `Tools` tab
*/
function tools_section(){
   
}


/**
*
*/
function initialize_tools(){
    global $_wp_using_ext_object_cache;
	setup_sjr_defaults( 'tools' );

    add_settings_section(
        'sjr-core_tools_section',                // section id
        'Tools',                                 // header title
         __NAMESPACE__.'\tools_section',         // Callback used to render the description of the section
        'sjr-core_tools'
    );

    //if( !$_wp_using_ext_object_cache ){
        add_settings_field(
            'Delete Transients',
            'Delete Transients',
             __NAMESPACE__.'\field_tools_transients',       // render ui
            'sjr-core_tools',                        // id for settings_fields
            'sjr-core_tools_section'                 // parent id
        );
    //}

    add_settings_field(
        'Delete Term #1',
        'Delete Term #1',
         __NAMESPACE__.'\field_tools_delete_term',       // render ui
        'sjr-core_tools',                        // id for settings_fields
        'sjr-core_tools_section'                 // parent id
    );

    add_settings_field(
        'Fix Broken Images',
        'Fix Broken Images',
         __NAMESPACE__.'\field_tools_fix_images',       // render ui
        'sjr-core_tools',                        // id for settings_fields
        'sjr-core_tools_section'                 // parent id
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_tools' );

/**
*   sanitize form data for saving options from `Tools` tab
*   @param array
*   @return array
*/
function sanitize_tools( $input ){
    if( wp_verify_nonce(filter_input(INPUT_POST, 'sjr-delete-transients'), 'sjr-delete-transients') )
        delete_transients();

    if( wp_verify_nonce(filter_input(INPUT_POST, 'sjr-delete-term'), 'sjr-delete-term') )
        delete_uncategorized();

    if( wp_verify_nonce(filter_input(INPUT_POST, 'sjr-fix-images'), 'sjr-fix-images') )
        run_image_fix( filter_input(INPUT_POST, 'sjr-fix-images-count') );

    return $input;
}