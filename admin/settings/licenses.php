<?php

namespace sjr;

/**
*   render description for `Licenses` tab
*/
function description_licenses(){
    echo '<p>Licenses settings</p>';
}

/**
*
*   @param array
*/
function field_license_bitly( $args ){
    $options = get_sjr_option( 'licenses' );

    echo render( 'admin/settings/licenses-bitly', $options );
}

/**
*
*   @param array
*/
function field_license_acfpro( $args ){
    $options = get_sjr_option( 'licenses' );

    echo render( 'admin/settings/licenses-acfpro', $options );
}

/**
*
*   @param array
*/
function field_license_akismet( $args ){
    $options = get_sjr_option( 'licenses' );

    echo render( 'admin/settings/licenses-akismet', $options );
}

/**
*   generic field for ftp fields
*   @param string identifier
*/
function field_license_ftp( $id = NULL ){
    $options = get_sjr_option( 'licenses' );
    $vars = object();

    if( $id ){
        $vars->id = $id.'_ftp';
    } else {
        $vars->id = 'ftp';
    }

    $vars->creds = isset( $options->{$vars->id} ) ? $options->{$vars->id} : $options->ftp;

    echo render( 'admin/settings/licenses-ftp', $vars );
}

/**
*
*   @param array
*/
function field_license_ithemes_sec( $args ){
    $options = get_sjr_option( 'licenses' );

    echo render( 'admin/settings/licenses-ithemes-sec', $options );
}

/**
*
*   @param array
*/
function field_license_offload_s3( $args ){
    $options = get_sjr_option( 'licenses' );

    echo render( 'admin/settings/licenses-offload-s3', $options );
}

/**
*
*   @param array
*/
function field_license_wpml( $args ){
    $options = get_sjr_option( 'licenses' );
    
    echo render( 'admin/settings/licenses-wpml', $options );
}

/**
*
*   @param array
*/
function field_license_wpmudev( $args ){
    $options = get_sjr_option( 'licenses' );
    
    echo render( 'admin/settings/licenses-wpmudev', $options );
}

/**
*
*   @param array
*/
function field_license_yandex( $args ){
    $options = get_sjr_option( 'licenses' );
    
    echo render( 'admin/settings/licenses-yandex', $options );
}

/**
*
*/
function initialize_licenses(){
    setup_sjr_defaults( 'licenses' );

    add_settings_section(
        'sjr-core_licenses_section',                          // section id
        'Licenses',                                           // header title
         __NAMESPACE__.'\description_licenses',          // Callback used to render the description of the section
        'sjr-core_licenses'
    );

    add_settings_field(
        'ACF Pro',
        'ACF Pro',
         __NAMESPACE__.'\field_license_acfpro',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'Akismet',
        'Akismet',
         __NAMESPACE__.'\field_license_akismet',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'Bitly',
        'Bitly',
         __NAMESPACE__.'\field_license_bitly',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'iThemes Security',
        'iThemes Security',
         __NAMESPACE__.'\field_license_ithemes_sec',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'Offload s3',
        'Offload s3',
         __NAMESPACE__.'\field_license_offload_s3',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'WPML',
        'WPML',
         __NAMESPACE__.'\field_license_wpml',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'WPMU Dev / Smush',
        'WPMU Dev / Smush',
         __NAMESPACE__.'\field_license_wpmudev',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );

    add_settings_field(
        'Yandex',
        'Yandex',
         __NAMESPACE__.'\field_license_yandex',       // render ui
        'sjr-core_licenses',                                  // id for settings_fields
        'sjr-core_licenses_section'                           // parent id
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_licenses' );

/**
*   sanitize form data for saving options from `Licenses` tab
*   @param array
*   @return array
*/
function sanitize_licenses( $input ){
    return $input;
}
