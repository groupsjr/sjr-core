<?php

namespace sjr;

// @TODO look into loading these on demand
require __DIR__.'/dev.php';
require __DIR__.'/images.php';
require __DIR__.'/licenses.php';
require __DIR__.'/ordering.php';
require __DIR__.'/site.php';
require __DIR__.'/taxonomies.php';
require __DIR__.'/tools.php';

/*
*   
*/
function admin_menu(){
    // @TODO find if this is needed, where does it show
    add_menu_page(
        '',                             // browser's title bar when the menu page is active
        'Core',                         // text of the menu in the administrator's sidebar
        'sjr-developer',                // roles able to access the menu
        'svr-developer_menu',           // id used to bind submenu items to this menu 
        ''                              // callback used to render this menu not used because sub page overrides
    );
    
    add_submenu_page(
        'svr-developer_menu',       // id of parent top-level menu page
        'SJR Dev',                  // title bar
        'Dev',                      // The label of this submenu item displayed in the menu
        'administrator',            // What roles are able to access this submenu item
        'sjr-core_dev',             // The ID used to represent this submenu item
         function(){
            controller( 'dev' );
        }
    );

    add_submenu_page(
        'svr-developer_menu',
        'SJR Images',
        'Images',
        'administrator',
        'sjr-core_images',
        function(){
            controller( 'images' );
        }
    );

    add_submenu_page(
        'svr-developer_menu',               // id of parent top-level menu page
        'SJR Licenses',         // title bar
        'Licenses',                  // The label of this submenu item displayed in the menu
        'administrator',                    // What roles are able to access this submenu item
        'sjr-core_licenses',                // The ID used to represent this submenu item
         function(){
            controller( 'licenses' );
        }
    );

    add_submenu_page(
        'svr-developer_menu',               // id of parent top-level menu page
        'SJR Post Ordering',         // title bar
        'Post Ordering',                  // The label of this submenu item displayed in the menu
        'administrator',                    // What roles are able to access this submenu item
        'sjr-core_ordering',                // The ID used to represent this submenu item
         function(){
            controller( 'ordering' );
        }
    );
    
    add_submenu_page(
        'svr-developer_menu',
        'SJR Site',
        'Site',
        'administrator',
        'sjr-core_site',
        function(){
            controller( 'site' );
        }
    );

    add_submenu_page(
        'svr-developer_menu',
        'SJR Taxonomies',
        'Taxonomies',
        'administrator',
        'sjr-core_taxonomies',
        function(){
            controller( 'taxonomies' );
        }
    );

    add_submenu_page(
        'svr-developer_menu',
        'SJR Tools',
        'Tools',
        'administrator',
        'sjr-core_tools',
        function(){
            controller( 'tools' );
        }
    );
}
add_action( 'admin_menu', __NAMESPACE__.'\admin_menu' );

/*
*   Renders a page to display for the theme menu defined above.
*/
function controller( $active_tab = 'index' ){
    settings_scripts();
    settings_styles();

    $vars = (object) array( 
        'active_tab' => $active_tab,
        'env' => !empty( $_GET['env'] ) ? $_GET['env'] : '', 
        'version' => version() 
    );

    wp_enqueue_style( 'sjr-admin-admin', plugins_url( 'public/admin/admin.css', dirname(__DIR__) ), 
                       array(), '' );

    switch( $active_tab ){
        case 'dev':
        case 'images':
        case 'licenses':
        case 'ordering':
        case 'site':
        case 'taxonomies':
        case 'tools':
            $vars->active_id = 'sjr-core_'.$active_tab;
            break;

        default:
            $vars->active_id = 'sjr-index';
            break;
    }

    // sort fields alphabetically 
    global $wp_settings_fields;
    foreach( $wp_settings_fields[$vars->active_id] as &$section ){
        uksort( $section, 'strnatcasecmp' );
    }

    echo render( 'admin/index', $vars );
}