<?php

namespace sjr;

/*
*   render description for `Ordering` tab
*/
function description_ordering(){
    echo '<p>Ordering settings</p>';
}


/*
*
*   @param array
*/
function field_enable_inline_sticky( $args ){
    $options = get_sjr_option( 'ordering' );

    echo render( 'admin/settings/ordering-inline-sticky', $options );
}

/*
*
*   @param array
*/
function field_enable_ordering_fixed( $args ){
    $page_types = fixed_order\page_types();
    sort( $page_types );

    $options = get_sjr_option( 'ordering' );

    $vars = array(
        'fixed' => $options->fixed,
        'page_types' => $page_types
    );

    echo render( 'admin/settings/ordering-fixed', $vars );
}

/*
*
*/
function initialize_ordering(){
    setup_sjr_defaults( 'ordering' );

    add_settings_section(
        'sjr-core_ordering_section',                          // section id
        'Ordering',                                           // header title
         __NAMESPACE__.'\description_ordering',          // Callback used to render the description of the section
        'sjr-core_ordering'
    );

    add_settings_field(
        'Fixed Positions',
        'Fixed Positions',
         __NAMESPACE__.'\field_enable_ordering_fixed',       // render ui
        'sjr-core_ordering',                                  // id for settings_fields
        'sjr-core_ordering_section'                           // parent id
    );

    add_settings_field(
        'Inline Sticky Posts',
        'Inline Sticky Posts',
         __NAMESPACE__.'\field_enable_inline_sticky',       // render ui
        'sjr-core_ordering',                                  // id for settings_fields
        'sjr-core_ordering_section'                           // parent id
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_ordering' );

/*
*   sanitize form data for saving options from `Ordering` tab
*   @param array
*   @return array
*/
function sanitize_ordering( $input ){
    return $input;
}

