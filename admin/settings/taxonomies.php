<?php

namespace sjr;

/**
*   render description for `Taxonomies` tab
*/
function description_taxonomies(){
    echo '<p>Taxonomy settings</p>';
}

/**
*   render settings UI for `Single Taxonomies`
*   show a checkbox for all taxonomies with  `show_ui` argument
*   @param array
*/
function field_taxonomies_single( $args ){
    $options = get_sjr_option( 'taxonomies' );
    
    $taxonomies = get_taxonomies( array(
        'show_ui' => TRUE
    ), 'objects' );

    // seems hacky, is to set custom taxonomies. look into option defaults
    $all = array_combine( array_keys($taxonomies), array_fill(0, count($taxonomies), 0) );
    $options->single = array_merge( $all, $options->single );

    echo render( 'admin/settings/taxonomies-single', array(
        'options' => $options,
        'taxonomies' => $taxonomies
    ) );
}

/**
*   Register settings fields for /wp-admin/admin.php?page=sjr-core_taxonomies
*/
function initialize_taxonomies(){
    setup_sjr_defaults( 'taxonomies' );

    add_settings_section(
        'sjr-core_taxonomies_section',                          // section id
        'Taxonomies',                                           // header title
         __NAMESPACE__.'\description_taxonomies',          // Callback used to render the description of the section
        'sjr-core_taxonomies'
    );

    add_settings_field(
        'Single Taxonomies',
        'Single Taxonomies',
         __NAMESPACE__.'\field_taxonomies_single',       // render ui
        'sjr-core_taxonomies',                                  // id for settings_fields
        'sjr-core_taxonomies_section'                           // parent id
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_taxonomies' );

/**
*   sanitize form data for saving options from `Taxonomies` tab
*   @param array
*   @return array
*/
function sanitize_taxonomies( $input ){
    return $input;
}