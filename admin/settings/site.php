<?php

namespace sjr;

/**
*   render description for `Site` tab
*/
function site_section(){
    echo '<p>Site settings</p>';
}

/**
*   
*   @param array
*/
function field_adminajax( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-adminajax', $options );
}

/**
*
*   @param array
*/
function field_adminbar( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-adminbar', $options );
}

/**
*
*   @param array
*/
function field_async( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-async', $options );
}

/**
*   
*   @param
*/
function field_dashboard_items( $args ){
    $options = get_sjr_option( 'site' );

    /*
    // @TODO dynamically get list of dashboard widgets to build toggles
    add_action( 'do_meta_boxes', function( $id = 'dashboard', $priority = '', $blank = '' ){
        global $wp_meta_boxes;

    }, PHP_INT_MAX - 1, 3 );
    */

    echo render( 'admin/settings/site-dashboard_items', $options );
}



/**
*
*   @param array
*/
function field_head( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-head', $options );
}

/**
*
*   @param array
*/
function field_log_email( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-log-email', $options );
}

/**
*
*   @param array
*/
function field_password( $args ){
    $options = get_sjr_option( 'site' );
    
    echo render( 'admin/settings/site-password', $options );
}

/**
*
*   @param array
*/
function field_posts_per_page( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-posts_per_page', $options );
}

/**
*
*   @param array
*/
function field_robots( $args ){
    $options = get_sjr_option( 'site' );

    $options->robots_hosts = implode( "\n", $options->robots_hosts );

    echo render( 'admin/settings/site-robots', $options );
}

/**
*
*   @param array
*/
function field_options( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-options', $options );
}

/**
*
*   @param array
*/
function field_title_tag( $args ){
    $options = get_sjr_option( 'site' );
    
    echo render( 'admin/settings/site-title-tags', $options );
}

/**
*
*   @param array
*/
function field_transients( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-transients', $options );
}

/**
*
*   @param array
*/
function field_validation( $args ){
    $options = get_sjr_option( 'site' );

    echo render( 'admin/settings/site-validation', $options );
}

/**
*
*/
function initialize_site(){
    setup_sjr_defaults( 'site' );

    add_settings_section(
        'sjr-core_site_section',                // section id
        'Site',                                 // header title
         __NAMESPACE__.'\site_section',         // Callback used to render the description of the section
        'sjr-core_site'
    );

    add_settings_field(
        'admin-ajax',
        'Admin Ajax',
         __NAMESPACE__.'\field_adminajax',       // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'admin-bar',
        'Admin Bar',
         __NAMESPACE__.'\field_adminbar',       // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'async-Javascript',
        'Async Javascript',
         __NAMESPACE__.'\field_async',          // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'dahsboard-items',
        'Dashboard Items',
         __NAMESPACE__.'\field_dashboard_items',// render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'head-elements',
        esc_html('<head> Elements'),
         __NAMESPACE__.'\field_head',       // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'log-email',
        'Log Email',
         __NAMESPACE__.'\field_log_email', // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'password-protection',
        'Password Protection',
         __NAMESPACE__.'\field_password',       // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'options',
        'Options',
         __NAMESPACE__.'\field_options',         // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'posts-per-page',
        'Posts Per Page',
         __NAMESPACE__.'\field_posts_per_page', // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'robots',
        'robots.txt',
         __NAMESPACE__.'\field_robots',         // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'title-tags',
        'Title Tags',
         __NAMESPACE__.'\field_title_tag',      // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'transients',
        'Transients',
         __NAMESPACE__.'\field_transients', // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );

    add_settings_field(
        'validation',
        'Validation',
         __NAMESPACE__.'\field_validation',         // render ui
        'sjr-core_site',                        // id for settings_fields
        'sjr-core_site_section'                 // parent id
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_site' );

/**
*   sanitize form data for saving options from `Site` tab
*   @param array
*   @return array
*/
function sanitize_site( $input ){
    foreach( $input as $key => $value ){
        
        switch( $key ){
            case 'adminajax':
                flush_rewrite_rules( FALSE );
                break;
            case 'password':
                // filter out empty user pass combos
                $input[$key]['users'] = array_values( array_filter($value['users'], function($creds){
                    return trim( $creds['user'] ) && trim( $creds['pass'] );
                }) );

                break;

            case 'posts_per_page':
                $input[$key] = array_map( 'intval', $value );
                break;

            case 'robots_hosts':
                $sites = explode( "\r\n", $value );
                $sites = array_map( 'trim', $sites );

                $input[$key] = implode( "\n", $sites );
                break;

            case 'transients':
                $on = (bool) $value;
                transients_db_check( $on );

                if( $on )
                    transients_db_check_obj_cache();
                
                break;

            default:
                break;
        }
    }

    return $input;
}

