<?php

namespace sjr;

/*
*   render description for `Dev` tab
*/
function description_dev(){
    echo '<p>Dev settings</p>'; 
}

/*
*
*   @param array
*/
function field_dev_deprecated( $args ){
    $options = get_sjr_option( 'dev' );

    echo render( 'admin/settings/dev-deprecated', $options );
}

/*
*
*   @param array
*/
function field_dev_revisions( $args ){
    $options = get_sjr_option( 'dev' );

    echo render( 'admin/settings/dev-revisions', $options );
}

/*
*   regsiter settings sections and fields for 'Dev' tab
*/ 
function initialize_dev(){
    setup_sjr_defaults( 'dev' );

    add_settings_section(
        'sjr-core_dev',                          // section id
        'Dev',                                           // header title
         __NAMESPACE__.'\description_dev',          // Callback used to render the description of the section
        'sjr-core_dev'
    );

    add_settings_field(
        'Die on deprecated function',
        'Die on deprecated function',
         __NAMESPACE__.'\field_dev_deprecated',       // render ui
        'sjr-core_dev',                             // id for settings_fields
        'sjr-core_dev'                           // parent id
    );

    add_settings_field(
        'Auto remove same revisions',
        'Auto remove same revisions',
         __NAMESPACE__.'\field_dev_revisions', // render ui
        'sjr-core_dev',                             // id for settings_fields
        'sjr-core_dev'                              // parent id
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_dev' );


/*
*   sanitize form data for saving options from `Dev` tab
*   @param array
*   @return array
*/
function sanitize_dev( $input ){
    foreach( $input as $key => $val ){
        $output[$key] = intval( $input[$key] );
    }

    return $output;
}
