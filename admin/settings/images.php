<?php

namespace sjr;

/*
*   render description for `Images` tab
*/
function description_images(){
    echo '<p>Images settings</p>';
}

/*
*   renders UI for 'Copy Images' section
*   @param array
*/
function field_copy_images( $args ){
    $options = get_sjr_option( 'images' );

    $wp_upload_dir = wp_upload_dir();
    $options->baseurl = $wp_upload_dir['baseurl'];

    $options->copy_images_environments = implode( "\n", $options->copy_images_environments );

    echo settings_render( 'admin/settings/images-copy', $options, $args['id'] );
}

/*
*
*   @param array
*/
function field_image_qstring( $args ){
    $options = get_sjr_option( 'images' );
    echo settings_render( 'admin/settings/images-query_string', $options, $args['id'] );
}



/*
*
*   @param array
*/
function field_offsite( $args ){
    $options = get_sjr_option( 'images' );
    $options->offsite_include = implode( "\n", $options->offsite_include );

    echo settings_render( 'admin/settings/images-offsite', $options, $args['id'] );
}

/*
*
*   @param array
*/
function field_placeholder( $args ){
    $options = get_sjr_option( 'images' );

    echo settings_render( 'admin/settings/images-placeholder', $options, $args['id'] );
}

/*
*   regsiter settings sections and fields for 'Images' tab
*/ 
function initialize_images(){
    setup_sjr_defaults( 'images' );

    add_settings_section(
        'sjr-core_images_section',                      // section id
        'Images',                                       // header title
         __NAMESPACE__.'\description_images',           // Callback used to render the description of the section
        'sjr-core_images'
    );

    add_settings_field(
        'images_copy',                                  // field id
        'Copy Images',                                  // field display title
         __NAMESPACE__.'\field_copy_images',            // render ui
        'sjr-core_images',                              // id for settings_fields
        'sjr-core_images_section',                      // parent id
        array( 'id' => 'images_copy',                   // callback args
               'options' => array('copy_images',         
                                  'copy_images_environments', 
                                  's3_copy_replace', 
                                  'copy_images_debug') )                                             
    );
    
    add_settings_field(
        'images_placeholder',
        'Placeholder Images',
         __NAMESPACE__.'\field_placeholder',
        'sjr-core_images',
        'sjr-core_images_section',                      // parent id
        array( 'id' => 'images_placeholder',            // callback args
               'options' => array('placeholder',         
                                  'placeholder_force') ) 
    );

    add_settings_field(
        'images_copy_offsite',
        'Copy Offsite Images',
         __NAMESPACE__.'\field_offsite',
        'sjr-core_images',
        'sjr-core_images_section',                      // parent id
        array( 'id' => 'images_copy_offsite',           // callback args
               'options' => array('offsite',         
                                  'offsite_include') ) 
    );

    add_settings_field(
        'images_querystring',
        'Image Query Strings',
         __NAMESPACE__.'\field_image_qstring',
        'sjr-core_images',
        'sjr-core_images_section',                      // parent id
        array( 'id' => 'images_querystring',            // callback args
               'options' => array('query_string',         
                                  'query_string_random') ) 
    );
}
add_action( 'admin_init', __NAMESPACE__.'\initialize_images' ); 

/*
*   sanitize form data for saving options from `Images` tab
*   @param array unsanitized collection of options
*   @return array the collection of sanitized values
*/
function sanitize_images( $input ){
    foreach( $input as $key => $value ){
        switch( $key ){
            case 'copy_images':
            case 'offsite':
            case 'placeholder':
            case 'placeholder_force':
                $input[$key] = intval( $input[$key] );
                break;

            case 'copy_images_environments':
            case 'offsite_include':
                $urls = explode( "\r\n", $input[$key] );
                $urls = array_map( 'trim', $urls );
                $urls = array_map( 'untrailingslashit', $urls );
                $urls = array_filter( $urls );

                // remove http(s):// from input
                if( $key == 'offsite_include' ){
                    $urls = array_map( function($url){
                        $host = parse_url( $url, PHP_URL_HOST );
                        return trim( $host ) ? $host : $url;
                    }, $urls );

                    // dont want to sort environments
                    sort( $urls );
                }

                $urls = array_unique( $urls );
                $urls = implode( "\n", $urls );

                $input[$key] = $urls;
                break;

            default:
                $input[$key] = $input[$key];
                break;
        }
    }

    return $input;
}
