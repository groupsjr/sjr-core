<?php

namespace sjr;

require __DIR__.'/settings/index.php';

/**
*	called on plugin activation
*/
function add_capabilities(){
	$role = get_role( 'administrator' );
	$role->add_cap( 'develop_sites' ); 
}

/**
*	remove welcome panel and dashboard items from admin index
*/
function dashboard_items(){
	$dashboard_items = get_sjr_option( 'site', 'dashboard_items' );

	if( !empty($dashboard_items['welcome_panel']) )
		remove_action( 'welcome_panel', 'wp_welcome_panel' );
}

/**
*	gets posts ordered by the meta key `_sjr_image_scan_last`, for `Fix Broken Images` Tool
*	@param array
*	@return array
*/
function get_image_replaced_posts( $params = NULL ){
	global $wpdb;

	$params = wp_parse_args( $params, array(
		'limit' => 10,
		'order' => 'DESC'
	) );

	$params['order'] = strcasecmp( $params['order'], 'ASC' ) === 0 ? 'ASC' : 'DESC';

	$sql = $wpdb->prepare( "SELECT P.ID, P.post_content, P.post_title, PM.meta_value AS `last_updated`
							FROM $wpdb->posts P
							LEFT JOIN $wpdb->postmeta PM 
								ON P.ID = PM.post_id 
								AND PM.meta_key = '_sjr_image_scan_last'
							WHERE P.post_status = 'publish'
							ORDER BY PM.meta_value {$params['order']}
							LIMIT %d", $params['limit'] );

	$res = $wpdb->get_results( $sql );
	return $res;
}

/**
*
*	@param string
*	@param array | object
*	@param string
*	@return string html
*/
function settings_render( $template, $vars = array(), $id = '' ){
	$html = render( $template, $vars );
	$html .= render( 'admin/settings/_env', array('id' => $id) );

	return $html;
}

/**
*	styles loaded on post editor
*/
function post_styles(){
	wp_enqueue_style( 'sjr-admin-post', plugins_url( 'public/admin/post.css', __DIR__ ), 
                       array(), '' );
}

/**
*	called from `Fix Broken Images` in Tools
*	@param int
*/
function run_image_fix( $limit = 10 ){
	$posts = get_image_replaced_posts( array(
		'limit' => $limit,
		'order' => 'ASC'
	) );

	foreach( $posts as $post )
		replace_broken_images( $post );
}

/**
*	javascripts loaded on settings page
*/
function settings_scripts(){
	wp_enqueue_script( 'sjr-admin-settings', plugins_url( 'public/admin/settings.js', SJR_CORE_FILE ), 
                        array('jquery'), '' );
}

/**
*	styles loaded on settings page
*/
function settings_styles(){
	wp_enqueue_style( 'sjr-admin-settings', plugins_url( 'public/admin/settings.css', SJR_CORE_FILE ), 
                       array(), '' );

	enqueue_font_awesome();
}

/**
*	render radio buttons for single taxonomy selector
*	@param array
*	@param int
*	@return
*/
function wp_terms_checklist_args( $args, $post_id ){
	$options = get_sjr_option( 'taxonomies' );

	$taxonomy = $args['taxonomy'];

	if( !empty($options->single[$taxonomy]) ){
		$args['checked_ontop'] = TRUE;
		$args['walker'] = new Walker_Taxonomy_Radio;
	}

	return $args;
}